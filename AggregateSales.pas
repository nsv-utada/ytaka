unit AggregateSales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DB, DBTables,
  Grids, Math;
  
type
  TfrmAggregateSales = class(TfrmMaster)
    Query1: TQuery;
    Query2: TQuery;
    Panel4: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    SG1: TStringGrid;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Button1: TButton;
    DTP1: TDateTimePicker;
    DTP2: TDateTimePicker;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
   	 procedure MakeSG;
  public
    { Public declarations }
  end;

  const
  
  //グリッドの列の定義

  CintItem         = 0;
  CintItemSum      = 1;
  CintEnd          = 2;
var
  frmAggregateSales: TfrmAggregateSales;

implementation
uses
Inter;
{$R *.dfm}

procedure TfrmAggregateSales.FormCreate(Sender: TObject);
begin
  top  := 5;
  Left := 5;
	Width := 700;
  Height := 800;
  DTP1.Date := Date();
  DTP2.Date := Date();
  Memo1.Text := '';
  Memo2.Text := '';
  Memo3.Text := '';
  Memo4.Text := '';      
  //ストリンググリッドの作成
  MakeSG;
end;

procedure TfrmAggregateSales.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
    Font.Color         := clNavy;
    
    ColWidths[CintItem]:= 320;
    Cells[CintItem, 0] := '仕入先';

    ColWidths[CintItemSum]:= 200;
    Cells[CintItemSum, 0] := '金額';

  end;
end;

procedure TfrmAggregateSales.Button1Click(Sender: TObject);
var
  sDateFrom    : String;
  sDateTo      : String;  
  sSql             : String;
  dSumshoukei      : Double;
  dSumshoukeiDenpyou      : Double;
  i                : Integer;
  j                : Integer;
  gRowNum          : Integer;
begin
  inherited;
  dSumshoukei := 0;
  dSumshoukeiDenpyou := 0;
  DateTimeToString(sDateFrom, 'yyyy/mm/dd', DTP1.Date);
  DateTimeToString(sDateTo, 'yyyy/mm/dd', DTP2.Date);

  //clear screen before 
  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
      end;
    end;
  end;

  Memo1.Text := '';
  Memo2.Text := '';
  Memo3.Text := '';
  Memo4.Text := '';


  sSql := 'SELECT sum(shoukei) as Sumshoukei FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE ';
    sSql := sSql + ' NouhinDate BETWEEN ''' + sDateFrom + '''';
    sSql := sSql + ' AND ';
    sSql := sSql + '''' + sDateTo + '''';

    with Query1 do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      while not EOF do begin
    	  Memo1.Text := FormatFloat('#,##', FieldbyName('Sumshoukei').AsFloat);
        dSumshoukeiDenpyou :=  FieldbyName('Sumshoukei').AsFloat;
        next
      end;
      Close;        
    end;

   // Make SQL
 	  sSql := 'SELECT D.ShiiresakiCode, sum(D.shoukei) as Sumshoukei, T.Name FROM ' + CtblTShiireDetail;
    sSql := sSql + ' D LEFT JOIN ' + CtblMShiiresaki + ' T ON';
    sSql := sSql + ' D.ShiiresakiCode = T.Code ';
    sSql := sSql + ' WHERE ';
    sSql := sSql + ' D.NouhinDate BETWEEN ''' + sDateFrom + '''';
    sSql := sSql + ' AND ';
    sSql := sSql + '''' + sDateTo + '''';
    sSql := sSql + ' AND ';
    sSql := sSql + ' D.shoukei != 0';
    sSql := sSql + ' AND ';
    sSql := sSql + ' D.ShiireShiharaiFlg = 0';
    sSql := sSql + ' Group By D.ShiiresakiCode, T.Name';
    sSql := sSql + ' ORDER BY D.ShiiresakiCode ';
    
    with Query2 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         ShowMessage('該当する仕入伝票はありません');
         MakeSG;
         Exit;
        end;
      // RowCountセット
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;
      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
      	  Cells[CintItem,           i] := FieldbyName('ShiiresakiCode').AsString + '.' + FieldbyName('Name').AsString;
      	  Cells[CintItemSum,      i] := FormatFloat('#,##', FieldbyName('Sumshoukei').AsFloat);
          dSumshoukei := dSumshoukei +  FieldbyName('Sumshoukei').AsFloat;
        end;
        next
      end;
      Close;
      Memo2.Text :=  FormatFloat('#,##', dSumshoukei);
      Memo3.Text :=  FloatToStr(SimpleRoundTo(dSumshoukei/dSumshoukeiDenpyou * 100, -2));
      Memo4.Text :=  FloatToStr(SimpleRoundTo((dSumshoukeiDenpyou-dSumshoukei)/dSumshoukeiDenpyou * 100, -2));
      //ShowMessage(round(iSumshoukei/iSumshoukeiDenpyou * 100)+' ');
     // Edit4.Text :=  FloatToStr(round((iSumshoukeiDenpyou-iSumshoukei)/iSumshoukeiDenpyou * 100));
    end;  
end;


procedure TfrmAggregateSales.SG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
type
  TVowels = set of char;

  
  var
	DRect  : TRect;
  dx: integer;
  Mode   : Integer;
  Text: string;
  Vowels : TVowels;
begin

    with SG1.Canvas do
    begin
      FillRect(Rect);
      Text := SG1.cells[aCol, aRow];
      dx := TextWidth(Text) + 2;
      TextOut(Rect.Right - dx, Rect.Top, Text);
    end;

    Vowels := [char(CintItemSum)];
		SG1.Canvas.FillRect(Rect);
	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;

	  if  (ARow = 0 ) then begin       //右寄せ

        Mode := DT_CENTER;

    end else begin
	    if  (char(ACol) IN Vowels) then begin       //右寄せ
     	  Mode := DT_RIGHT;
  	  end else begin
    		Mode := DT_LEFT;
      end;

    end;

  	DrawText(SG1.Canvas.Handle, PChar(SG1.Cells[ACol,ARow]),
          Length(SG1.Cells[ACol,ARow]), DRect, Mode);

end;

end.
