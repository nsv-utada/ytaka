unit DM1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, Inter;

type
  TfrmDM1 = class(TDataModule)
    QueryMakeCB: TQuery;
    QuerySelect: TQuery;
    Query2JuuCheck: TQuery;
    QueryUpdate: TQuery;
    QueryInsert: TQuery;
    QueryDelete: TQuery;
    QueryGetUser: TQuery;
    QueryCount: TQuery;
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

procedure MakeCBAll(myCB:TComboBox; myTable, myCode, myName:String);


  //汎用的な関数////////////////////////////////////////////////////////////
  //２重登録のチェック
	function Check2Juu(sTableName, sFieldName, sContent, sID: String) : String;
	function Check2Juu3(sTableName, sRetField, sWhere: String) : String;
	//function Check2Juu2(sTableName, sFieldName, sContent, sID: String) : String;
  //レコードの削除
	//function DeleteRecord(sTableName, sFieldName, sContent : String):Boolean;
	function DeleteRecord(InRecArry:TInRecArry; sTableName : String):Boolean;
  //レコードの挿入
	function InsertRecord(InRecArry:TInRecArry; sTableName : String):Boolean;
  //レコードの修正
	//function UpdateRecord(InRecArry:TInRecArry; sTableName : String):Boolean;
	function UpdateRecord2(InRecArry:TInRecArry; sTableName, sWhere : String):Boolean;
  //特定のテーブルから特定のフィールドデータを文字型で返す
  function GetFieldData(sTableName, sReturnFieldName, sWhere : String):String;

  //レコード数を返す
  function GetCount(sTableName, sWhere: String) : integer;

  //ログインユーザー名を取得する
  function GetUser : String;


var
  frmDM1: TfrmDM1;

implementation

{$R *.DFM}


//ログインユーザー名を取得する
function GetUser : String;
var
	sSql, sUName : String;
begin
	//ユーザー名の取得
  sSql := 'SELECT UName = user_name()';
  sSql := 'SELECT abc=convert(char(30), CURRENT_USER)';
  with frmDM1.QueryGetUser do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //sUName := Fields[1].AsString;
    sUName := frmDM1.QueryGetUser.FieldByName('abc').asString;
    Close;
  end;
  if sUName = 'arai2' then begin
    sUName := 'arai2';
  	GPass := 'arai2';
  end else if sUName = 'kei' then begin
    sUName := 'keitakahashi';
  	GPass := 'keit';
  end else if sUName = 'test2' then begin
    sUName := 'test';
  	GPass := 'test';
  end;

  result := sUName;
end;

//特定のテーブル(sTableName)からWHERE句(sWhere)の
//特定のフィールド(sReturnFieldName)のデータを
//文字型で返す
//平均などにも対応
function GetFieldData(sTableName, sReturnFieldName, sWhere : String):String;
var
	sSql : String;
  sReturnFieldName2 : String;
begin
	if Pos('=', sReturnFieldName) > 0 then begin
  	sReturnFieldName2 := Trim(Copy(sReturnFieldName, 1, Pos('=', sReturnFieldName)-1));
  end else begin
  	sReturnFieldName2 := sReturnFieldName;
  end;

	sSql := 'SELECT ' + sReturnFieldName + ' FROM ' + sTableName;
  if sWhere <> '' then
	  sSql := sSql + sWhere;
	with frmDM1.QuerySelect do begin
 		Close;
   	Sql.Clear;
    Sql.Add(sSql);
 	  Open;
    if not EOF then begin
	    result := FieldByName(sReturnFieldName2).AsString;
    end else begin
      result := '';
    end;
    Close;
  end;
end;

//特定のデータを削除する
//レコード挿入用の配列を使う
// 	type TInRecArry = array[1..100, 1..3] of String;
//  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
//
//フィールドのマックスは100
//成功した場合には、Trueを返す
function DeleteRecord(InRecArry:TInRecArry; sTableName : String):Boolean;
var
	sSql : String;
  i : Integer;
begin
	sSql := 'Delete FROM ' + sTableName;
  sSql := sSql + ' WHERE ';
  i := 1;
  sSql := sSql + InRecArry[i, 1] + '=' + InRecArry[i, 3] + InRecArry[i, 2] + InRecArry[i, 3];
  i := 2;
  while InRecArry[i, 1] <> 'END' do begin
	  sSql := sSql + ' AND ';
	  sSql := sSql + InRecArry[i, 1] + '=' + InRecArry[i, 3] + InRecArry[i, 2] + InRecArry[i, 3];
    i := i + 1;
  end;

  try
		with frmDM1.QueryDelete do begin
  		Close;
    	Sql.Clear;
	    Sql.Add(sSql);
  	  ExecSql;
      result := True;
	  end;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
    	//frmDMMaster.DBShokuzai.RollBack;
		  ShowMessage('削除に失敗しました');
      result := False;
    end
  end;//of try
end;


//function InsertRecord(InRecArry:array of String; sTableName : String):Boolean;
//
//レコード挿入用の配列を使う
// 	type TInRecArry = array[1..100, 1..3] of String;
//  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
//
//フィールドのマックスは100
function InsertRecord(InRecArry:TInRecArry; sTableName : String):Boolean;
var
	sSql : String;
  i : Integer;
begin
	sSql := 'INSERT INTO ' + sTableName;
  sSql := sSql + ' (';

  i := 1;
  while InRecArry[i, 1] <> 'END' do begin
	  sSql := sSql + InRecArry[i, 1] + ',';
  	i := i + 1;
  end;
  sSql := Copy(sSql, 1, Length(sSql)-1);
  sSql := sSql + ' ) VALUES (';
  i := 1;
  while InRecArry[i, 1] <> 'END' do begin
	  sSql := sSql + InRecArry[i, 3] + InRecArry[i, 2] + InRecArry[i, 3] +',';
  	i := i + 1;
  end;
  //最後のカンマをとる
  sSql := Copy(sSql, 1, Length(sSql)-1);
  sSql := sSql + ' )';

  try
		with frmDM1.QueryInsert do begin
  		Close;
    	Sql.Clear;
	    Sql.Add(sSql);
  	  ExecSql;
		  //ShowMessage('登録しました');
      result := True;
	  end;
  except
	  on E: EDBEngineError do begin
    	//ShowMessage(E.Message);
    	//frmDMMaster.DBShokuzai.RollBack;
		  ShowMessage('登録に失敗しました' + InRecArry[1, 1]);
      result := False;
    end
  end;//of try

end;

//レコード挿入用の配列を使う
// 	type TInRecArry = array[1..100, 1..3] of String;
//  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
//注意!!!
//	WHERE句　  TInRecArry[1, 1-3]はWHERE句で使う
//      TInRecArry[1, 1] = 'ALL'の時は、WHERE句はつけない（すべてのレコードを更新する）
//フィールドのマックスは100
//WHERE句を柔軟に
function UpdateRecord2(InRecArry:TInRecArry; sTableName, sWhere : String):Boolean;
var
	sSql : String;
  i : Integer;
begin
	sSql := 'UPDATE ' + sTableName;
  sSql := sSql + ' SET ';

  i := 2; //ここがポイント
  while InRecArry[i, 1] <> 'END' do begin
	  sSql := sSql + InRecArry[i, 1] + '=' +
                  InRecArry[i, 3] + InRecArry[i, 2] + InRecArry[i, 3];
    sSql := sSql + ',';
  	i := i + 1;
  end;
  sSql := Copy(sSql, 1, Length(sSql)-1);

  //WHERE句の作成
  if InRecArry[1, 1] = 'ALL' then begin
  end else begin
	  sSql := sSql + sWHERE;
  end;

  try
		with frmDM1.QueryUpdate do begin
  		Close;
    	Sql.Clear;
	    Sql.Add(sSql);
  	  ExecSql;
      result := True;
	  end;
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
    	//frmDMMaster.DBShokuzai.RollBack;
		  ShowMessage('更新に失敗しました');
      result := False;
    end
  end;//of try
end;


//データの２重登録をチェックする
//あればsIDで指定された項目を返す返す。
//なければ''を返す
function  Check2Juu(sTableName, sFieldName, sContent, sID: String) : String;
var
	sSql : String;
begin
	sSql := 'SELECT ' + sID + ' FROM ' + sTableName;
  sSql := sSql + ' WHERE ' + sFieldName + ' = ''' + sContent + '''';

	with frmDM1.Query2JuuCheck do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
   	Result := '';
    while not EOF do begin
    	Result := FieldByName(sID).AsString;
      Next;
    end;
    Close;
  end;
end;

//データの２重登録をチェックする
//あればsRetFieldで指定された項目を返す返す。
//なければ''を返す
function Check2Juu3(sTableName, sRetField, sWhere: String) : String;
var
	sSql : String;
begin
	sSql := 'SELECT ' + sRetField + ' FROM ' + sTableName;
  sSql := sSql + sWhere;

	with frmDM1.Query2JuuCheck do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
   	Result := '';
    while not EOF do begin
    	Result := FieldByName(sRetField).AsString;
      Next;
    end;
    Close;
  end;
end;

//コンボボックスを作成
//2000/12/23 Hajime kubota
procedure MakeCBAll(myCB:TComboBox; myTable, myCode, myName:String);
var
	sSql : String;
begin
  myCB.Items.Clear;

  sSql := 'SELECT ' + myCode + ',' + myName;
  sSql := sSql + ' FROM ' + myTable;
  sSql := sSql + ' ORDER BY ' + myCode;
  with frmDM1.QueryMakeCB do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
    	myCB.Items.Add(FieldByName(myCode).AsString + ',' + FieldByName(myName).AsString);
    	Next;
    end;//of while
    Close;
  end;//of with
end;

function GetCount(sTableName, sWhere: String) : integer;
var
  sSql : String;
begin
  sSql := 'SELECT t=count(*) FROM ' + sTableName;
  if sWhere <> '' then
    sSql := sSql + sWhere;

  with frmDM1.QueryCount do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
	    result := FieldByName('t').AsInteger;
    end else begin
      result := 0;
    end;
    Close;
  end;
end;


end.
