inherited frmGennkinnKaishuu: TfrmGennkinnKaishuu
  Left = 281
  Top = 49
  VertScrollBar.Range = 0
  BorderStyle = bsSingle
  Caption = #29694#37329#27531#39640#22238#21454#20837#21147#30011#38754
  ClientHeight = 459
  ClientWidth = 507
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 507
    Height = 33
    inherited Label1: TLabel
      Top = 9
      Width = 124
      Height = 16
      Caption = #29694#37329#22238#21454#12481#12455#12483#12463
      Font.Height = -16
    end
    inherited Label2: TLabel
      Top = 16
    end
  end
  inherited Panel2: TPanel
    Top = 399
    Width = 507
    inherited BitBtn1: TBitBtn
      Left = 400
      Height = 23
      Caption = #38281#12376#12427
    end
    inherited BitBtn2: TBitBtn
      Left = 176
      Width = 105
      Height = 23
      Caption = #22238#21454#28168
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 288
      Top = 8
      Width = 105
      Height = 23
      Caption = #22238#21454#28168#21462#28040
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
      Kind = bkRetry
    end
  end
  inherited Panel3: TPanel
    Top = 33
    Width = 507
    Height = 366
    object Label4: TLabel
      Left = 109
      Top = 71
      Width = 277
      Height = 12
      Caption = #12424#12415#12434#20837#21147#12375#12390'F1'#12461#12540#12434#25276#19979' or '#31777#26131#26908#32034#12508#12479#12531#12434#25276#19979
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object Label3: TLabel
      Left = 304
      Top = 14
      Width = 191
      Height = 24
      Caption = #32013#21697#26085#12392#24471#24847#20808#21517#12434#25351#23450#12375#12390#65292#20253#31080#30058#21495#26908#32034#12508#12479#12531#12434#25276#19979#12375#12390#19979#12373#12356#65294
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object Label5: TLabel
      Left = 113
      Top = 118
      Width = 274
      Height = 12
      Caption = #20253#31080#30058#21495#12434#25351#23450#12375#12390#20253#31080#34920#31034#12508#12479#12531#12434#25276#19979#12375#12390#19979#12373#12356#65294
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel
      Left = 9
      Top = 334
      Width = 103
      Height = 12
      Caption = '('#20840#35282#12391#26368#22823'50'#25991#23383')'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object Label7: TLabel
      Left = 357
      Top = 279
      Width = 48
      Height = 12
      Caption = '( '#31246#36796#12415')'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object pnlTokuisakiName: TPanel
      Left = 16
      Top = 44
      Width = 89
      Height = 20
      Caption = #24471#24847#20808#21517
      TabOrder = 1
    end
    object pnlDennpyouCode: TPanel
      Left = 16
      Top = 92
      Width = 89
      Height = 20
      Caption = #20253#31080#30058#21495
      TabOrder = 2
    end
    object pnlNouhinDate: TPanel
      Left = 16
      Top = 12
      Width = 89
      Height = 20
      Caption = #32013'  '#21697'  '#26085
      TabOrder = 3
    end
    object dtpNouhinDate: TDateTimePicker
      Left = 112
      Top = 12
      Width = 185
      Height = 20
      Date = 37297.655360127300000000
      Time = 37297.655360127300000000
      TabOrder = 4
    end
    object cmbTokuisakiName: TComboBox
      Left = 112
      Top = 42
      Width = 185
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 5
      OnExit = cmbTokuisakiNameExit
      OnKeyDown = cmbTokuisakiNameKeyDown
    end
    object cmbDennpyouBanngou: TComboBox
      Left = 112
      Top = 90
      Width = 185
      Height = 20
      ItemHeight = 12
      TabOrder = 0
      OnKeyDown = cmbDennpyouBanngouKeyDown
    end
    object cmdKanniKennsaku: TButton
      Left = 304
      Top = 44
      Width = 65
      Height = 17
      Caption = #31777#26131#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = cmdKanniKennsakuClick
    end
    object cmdDennyouBanngouKennsaku: TButton
      Left = 384
      Top = 44
      Width = 105
      Height = 17
      Caption = #20253#31080#30058#21495#26908#32034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = cmdDennyouBanngouKennsakuClick
    end
    object cmdDennpyouHyouji: TButton
      Left = 304
      Top = 92
      Width = 65
      Height = 17
      Caption = #20253#31080#34920#31034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = cmdDennpyouHyoujiClick
    end
    object pnlBar: TPanel
      Left = 16
      Top = 159
      Width = 473
      Height = 5
      TabOrder = 9
    end
    object pnlNouhinDate2: TPanel
      Left = 16
      Top = 180
      Width = 89
      Height = 20
      Caption = #32013' '#21697' '#26085
      TabOrder = 10
    end
    object pnlDennpyouBanngou2: TPanel
      Left = 16
      Top = 212
      Width = 89
      Height = 20
      Caption = #20253#31080#30058#21495
      TabOrder = 11
    end
    object pnlTokuisakiName2: TPanel
      Left = 16
      Top = 244
      Width = 89
      Height = 20
      Caption = #24471#24847#20808#21517
      TabOrder = 12
    end
    object pnlKinngaku2: TPanel
      Left = 16
      Top = 276
      Width = 89
      Height = 20
      Caption = #37329#12288#12288#38989
      TabOrder = 13
    end
    object pnlMemo: TPanel
      Left = 16
      Top = 308
      Width = 89
      Height = 20
      Caption = #12513#12288#12288#12514
      TabOrder = 14
    end
    object edbNouhinDate2: TEdit
      Left = 119
      Top = 179
      Width = 130
      Height = 24
      Color = clMenu
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
    end
    object edbDennpyouBanngou2: TEdit
      Left = 119
      Top = 211
      Width = 226
      Height = 24
      Color = clMenu
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
    end
    object edbTokuisakiName2: TEdit
      Left = 119
      Top = 243
      Width = 226
      Height = 24
      Color = clMenu
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 17
    end
    object edbKinngaku2: TEdit
      Left = 119
      Top = 275
      Width = 226
      Height = 24
      Color = clWhite
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 18
    end
    object memMemo: TMemo
      Left = 120
      Top = 304
      Width = 369
      Height = 41
      Lines.Strings = (
        'memMemo')
      MaxLength = 100
      TabOrder = 19
    end
    object pnlflg: TPanel
      Left = 264
      Top = 179
      Width = 89
      Height = 20
      Caption = #22238#21454#28168'/'#26410#22238#21454
      TabOrder = 20
    end
    object edbflg: TEdit
      Left = 359
      Top = 179
      Width = 130
      Height = 24
      Color = clMenu
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 21
    end
    object pnlKaishuubi: TPanel
      Left = 360
      Top = 212
      Width = 89
      Height = 20
      Caption = #22238#12288#21454#12288#26085
      TabOrder = 22
    end
    object dtpKaishuubi: TDateTimePicker
      Left = 360
      Top = 244
      Width = 129
      Height = 20
      Date = 37297.655360127300000000
      Time = 37297.655360127300000000
      TabOrder = 23
      OnChange = dtpKaishuubiChange
    end
  end
  inherited SB1: TStatusBar
    Top = 440
    Width = 507
  end
  object qryGennkinnKaishuu: TQuery
    DatabaseName = 'taka'
    Left = 432
    Top = 129
  end
end
