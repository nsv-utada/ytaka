unit InfoMart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Master,
  Dialogs, DB, DBTables, StdCtrls, Buttons, ExtCtrls, ComCtrls, FMTBcd,
  SqlExpr;

type
  TfrmInfoMart = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    btnRegister: TBitBtn;
    CBNyuuryokusha: TComboBox;
    Query2: TQuery;
    btnChooseFile: TBitBtn;
    Label5: TLabel;
    ListBox1: TListBox;
    Label6: TLabel;
    Edit1: TEdit;
    txtFilePath: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    dlgOpenFile: TOpenDialog;
    QueryDenpyou: TQuery;
    dabDenpyou: TDatabase;
    Label10: TLabel;
    DTP1: TDateTimePicker;
    
	  procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnChooseFileClick(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
	  procedure Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
    procedure BitBtn3Click(Sender: TObject);

  private
    { Private declarations }
    Function InsertDenpyou(DenpyouDetail:TStringList; strDenpyouBangou:String): Boolean;
    Function InsertDenpyouDetail(DenpyouDetail:TStringList; strDenpyouBangou:String) : Boolean;
    Function DeleteAll(strInfoDenpyouNo:String): Boolean;
    Function UpdateItem(DenpyouDetail:TStringList) : Boolean;
	  Function GetDenpyouBanngou(strTokuisakiCode1:String; strGDate: String) : String;
    Function GetAriaCode(sTokuisakiCode1:string) : String;
    Function Replace (Str:string; Before:string; After: string): String;
    Function GetGenka(sCode1,sCode2:String):String;
    Function GetTanka(sCode1,sCode2,sTokuisakiCode:String):String;
    Function UpdateDenpyou(strDenpyouBangou:String) : Boolean;
    Function UpdateGenkinDenpyou(strDenpyouBangou:String) : Boolean;

  public
    { Public declarations }
  end;

var
  frmInfoMart: TfrmInfoMart;
  //gDenpyouNo  : String;
  curSum      : Currency; 

implementation
uses
Inter, DenpyouPrint, HKLib, ListInfoMart, Denpyou2;

{$R *.dfm}

procedure TfrmInfoMart.FormCreate(Sender: TObject);
begin
  inherited;
  Top  := 5;
  Left := 5;
	Width := 650;
  Height := 480;
  DTP1.Date := Date();  
end;

procedure TfrmInfoMart.FormActivate(Sender: TObject);
begin
  inherited;
  if CBNyuuryokusha.Text ='' then begin
	  CBNyuuryokusha.SetFocus;
    CBNyuuryokusha.Items.LoadFromFile('nyuuryokusha.txt');
  end;
end;

procedure TfrmInfoMart.btnChooseFileClick(Sender: TObject);
begin
    dlgOpenFile := TOpenDialog.Create(Self);
    dlgOpenFile.Filter := 'Only Csv files (*.csv)|*.csv';
    if dlgOpenFile.Execute then begin
        txtFilePath.Text:=dlgOpenFile.FileName;
    end;
    dlgOpenFile.Free;
    ChDir(ExtractFilePath(Application.ExeName));
    
end;

procedure TfrmInfoMart.btnRegisterClick(Sender: TObject);
  var dlg           : TForm;
      res           : Word;
      i             : Integer;
      j             : Integer;      
      sFileName     : String;
      slCsv, slRow  : TStringList;
      sCurrentNo    : String;
      sNewNo        : String;
      iCommit       : Boolean;
      sInfoDenpyouNo: String;
      iRollback     : Boolean;
      strDenpyouBangou :String;
      strTokuisakiCode1 :String;
      strNouhinDate :String;
      strCode           : TStringList;
      strCode1          : String;
      strCode2          : String;
      sLockFileName     : String;
      strAriaCode       : String;
      strTannka: String;
      FileHandle: Integer;
      LArrayOfDenpyouCode : array[0..9999] of String;
begin
    ListBox1.Items.Clear;

    if Trim(CBNyuuryokusha.Text) = '' then
    begin
      ShowMessage('入力者を選択して下さい。');
      Exit;
    end;


    sFileName:=txtFilePath.Text;

    if Trim(sFileName) = '' then
    begin
      ShowMessage('データファイルを選択して下さい。');
      Exit;
    end;

     //確認メッセージ
    dlg := CreateMessageDialog('登録しますか', mtInformation,
                             [mbYes, mbNo]);
    dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
    res := dlg.ShowModal;
    if res <> mrYes then begin
      exit;
    end;
    dlg.Free;



    //ALL CSV FILES
    slCsv :=  TStringList.Create;
    slCsv.LoadFromFile(sFileName);

    //ONE ROWS
    slRow := TStringList.Create;

 try

    // DATA START FROM ROW 2 IN CSV FILES
    dabDenpyou.Open;
    if not dabDenpyou.InTransaction then begin
       dabDenpyou.StartTransaction;
    end;
    iCommit   := True;
    iRollback := False;


    sInfoDenpyouNo := '';
    Split(',', slCsv[2], slRow);

    sCurrentNo:=slRow[2];

    Split(',', slCsv[2], slRow);
    sCurrentNo:=slRow[2];


    j := 0;

    for i:=2 to slCsv.Count-1 do begin
         Split(',', slCsv[i], slRow);
         if (slRow[0] = 'D') then begin

            //sNewNo:=slRow[2];
            sNewNo:=slRow[43];

            //if 自社管理商品コード OR  取引先コード will rollback all and show message in textarea
            if(Trim(slRow[7]) = '')  then begin
                ListBox1.Items.Add(IntToStr(i + 1) + '行目：［取引先コード］が空の為登録できません。［伝票No］' + sNewNo);
                ListBox1.Items.Add('すべての登録処理をキャンセルします。インフォマートデータファイルを確認して下さい。');
                ListBox1.Update;
                iRollback := True;
                dabDenpyou.Rollback;
                Exit;
            end;

            strCode := TStringList.Create;
            strCode1 :='';
            strCode2 :='';
            if (Trim(slRow[16]) <> '' )then begin
                Split('-', slRow[16], strCode); //自社管理商品コード
                strCode1 := strCode[0];
                if(strCode.Count > 1) then begin
                    strCode2          := strCode[1];
                end;
            end;


            if(Trim(strCode1) <> '') OR (Trim(strCode2) <> '' )  then begin
                 if(Length(strCode1) <> 2 ) OR (Length(strCode2) <> 4 )  then begin
                    ListBox1.Items.Add(IntToStr(i + 1) + '行目：［自社管理商品コード］が不正の為登録できません。［伝票No］' + sNewNo);
                    ListBox1.Items.Add('すべての登録処理をキャンセルします。インフォマートデータファイルを確認して下さい。');
                    ListBox1.Update;
                    iRollback := True;
                    dabDenpyou.Rollback;
                    Exit;
                end;
            end;

            //if(Trim(slRow[7]) <> '') AND (Trim(slRow[16]) <> '') then begin
            if(Trim(slRow[7]) <> '') then begin

              //START INSERT TO DATABASE
              if(i = 2) OR (sCurrentNo<>sNewNo)  then begin


                  ListBox1.Items.Add('伝票No :[ ' + sNewNo + ' ] 登録');
                  ListBox1.Update;

                  strTokuisakiCode1 := slRow[7];

                  strAriaCode:=GetAriaCode(strTokuisakiCode1);

                  //ロックファイル名の生成
                  sLockFileName :=  'Lock_' + strAriaCode + '.txt';

                  //2003.08.03
                 //ロックファイルがあるかどうか
                  if FileExists(sLockFileName)=True then begin
	                    ShowMessage('現在他のユーザーがエリアコード' + strAriaCode + 'の伝票番号を取得中です。数秒お待ちください');
                          ListBox1.Items.Add('すべての登録処理をキャンセルします。');
                          ListBox1.Update;
                          iRollback := True;
                          dabDenpyou.Rollback;
                          dabDenpyou.Close;
                          exit;
                  end else begin
	                    //ここでロックファイルを作成する
                      FileHandle := FileCreate(sLockFileName);
	                    if FileHandle = -1 then begin
    	                    Showmessage('このコースには現在ロックがかかっています。再度時間をおいて登録してください。');
                          ListBox1.Items.Add('すべての登録処理をキャンセルします。');
                          ListBox1.Update;
                          iRollback := True;
                          dabDenpyou.Rollback;
                          dabDenpyou.Close;
                          Exit;
                      end else begin
	                        FileClose(FileHandle);
                     end;
                  end;

                //GET trDenpyouBangou

                 // strNouhinDate := slRow[37];   // mod before 20130227 utada
                  strNouhinDate :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada

                  strDenpyouBangou := GetDenpyouBanngou(strTokuisakiCode1,strNouhinDate);

                  LArrayOfDenpyouCode[j] :=strDenpyouBangou;
                  j := j + 1;


                  if strDenpyouBangou = '' then begin
                     ListBox1.Items.Add('伝票番号が取得出来ませんでした。');
                     ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                     ListBox1.Update;
                     iRollback := True;
                     dabDenpyou.Rollback;
                     dabDenpyou.Close;
                     DeleteFile(sLockFileName);

                     Exit;
                  end;

                  sCurrentNo:=sNewNo;


                  if(InsertDenpyou(slRow, strDenpyouBangou) = False) OR (strDenpyouBangou = '') then begin
                     ListBox1.Items.Add('伝票登録に失敗しました。');
                     ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                     ListBox1.Update;
                     iRollback := True;
                     dabDenpyou.Rollback;
                     dabDenpyou.Close;
                     DeleteFile(sLockFileName);
                     Exit;
                  end;

                  if DeleteFile(sLockFileName) = true then begin
                  ;
                  end else begin
 	                  ShowMessage('伝用番号のロックファイル' + sLockFileName + 'がありませんでした。伝票がきちんと登録されているかどうか確認してください');
                  end;


              end;


              if(InsertDenpyouDetail(slRow, strDenpyouBangou) = False) then begin
                  ListBox1.Items.Add('伝票明細の登録に失敗しました。');
                  ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                  ListBox1.Update;
                  iRollback := True;
                  dabDenpyou.Rollback;
                  dabDenpyou.Close;
                  Exit;
              end;


		          if(UpdateItem(slRow) = False) then begin
                 ListBox1.Items.Add('商品の更新処理に失敗しました。');
                 ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
                 ListBox1.Update;
                 iRollback := True;
                 dabDenpyou.Rollback;
                 dabDenpyou.Close;
                 Exit;
              end;


         end;
      end;

    end;


    //add 20190316 by utada  得意先マスタの価格優先対応
    j := 0;
    ListBox1.Items.Add('伝票の再計算処理中');
    ListBox1.Update;
    while LArrayOfDenpyouCode[j] <> '' do begin


		    strDenpyouBangou := LArrayOfDenpyouCode[j];

		    if(UpdateDenpyou(strDenpyouBangou) = False) then begin
		         ListBox1.Items.Add('伝票の更新処理に失敗しました。');
		         ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
		         ListBox1.Update;
		         iRollback := True;
		         dabDenpyou.Rollback;
		         dabDenpyou.Close;
		         Exit;
		    end;

		        //消費税の更新  add utada
		    if(UpdateGenkinDenpyou(strDenpyouBangou) = False)  then begin
		         ListBox1.Items.Add('現金伝票の更新処理に失敗しました。');
		         ListBox1.Items.Add('すべての登録処理をキャンセルします。管理者へ連絡して下さい。');
		         ListBox1.Update;
		         iRollback := True;
		         dabDenpyou.Rollback;
		         dabDenpyou.Close;
		         Exit;
		    end;

	      j := j + 1;

    end;    

    ListBox1.Items.Add('伝票の再計算処理が完了しました。');
    ListBox1.Update;

    if(iRollback = True) then
     begin
        dabDenpyou.Rollback;
     end
    else if(iCommit = True) then
     begin
        dabDenpyou.Commit;
        ShowMessage('登録が完了しました。');
     end;

    		Beep();
        dabDenpyou.Close;


 except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  ShowMessage(E.Message);
      dabDenpyou.Close;
      Beep();
    end;
  end;

end;



procedure TfrmInfoMart.BitBtn3Click(Sender: TObject);
begin
  inherited;
  TfrmListInfoMart.Create(Self);
end;

procedure TfrmInfoMart.Split(const Delimiter: Char;Input: string;const Strings: TStrings) ;
begin
	Assert(Assigned(Strings)) ;
	Strings.Clear;
	Strings.Delimiter := Delimiter;
	Strings.DelimitedText := Replace(Input, ' ', '') ;
end;





Function TfrmInfoMart.InsertDenpyou(DenpyouDetail:TStringList; strDenpyouBangou:String): Boolean;
  var sSql              : String;

      strTokuisakiCode1 : String;
      strTokuisakiCode2 : String;
      strInputDate      : String;
      strDdate          : String;
      strUriageKingaku  : String;
      strGatsubun       : String;
      strDenpyouCode    : String;
      strMemo           : String;
      strTeiseiflg			: String;
      strBikou          : String;
      strInfoDenpyouNo  : String;

      strNouhinDate     : String;
      strShoukei        : String;
      strShouhizei      : String;
      strGoukei         : String;

      curShouhizei      : Currency;
      curGoukei         : Currency;
      douTax            : Double;   //2014.04.01 消費税対応

begin
    strTokuisakiCode1 := DenpyouDetail[7];  //取引先コード
    strTokuisakiCode2 := '';
    DateTimeToString(strInputDate, 'yyyy/mm/dd', now);
    //strDdate          := DenpyouDetail[37];  //納品日 mod before 20130227 utada
    strDdate          :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada

    //strUriageKingaku  := DenpyouDetail[34];  //総合計
    //strUriageKingaku  := DenpyouDetail[29];  //［合計 商品本体］
    strUriageKingaku  := '0';  //［合計 商品本体］ /add 20190316 by utada  得意先マスタの価格優先対応 後で再計算する為不要


    DateTimeToString(strGatsubun, 'yyyy/mm/01', StrToDateTime(strDdate));
    strMemo           := '';
    strTeiseiflg			:= '0';
    strBikou          := Edit1.Text;
    strInfoDenpyouNo  := trim(DenpyouDetail[2]);

    //strShoukei        := trim(DenpyouDetail[34]);//総合計
    strShoukei        := trim(DenpyouDetail[29]);//［合計 商品本体］

    if strShoukei='' then begin
       strShoukei:='0';
    end;

    if strUriageKingaku='' then begin
       strUriageKingaku:='0';
    end;

    //2014.04.01 消費税対応 begin
    if strDdate < CsChangeTaxDate then begin
      douTax :=  CsOldTaxRate;
    end else begin
      douTax :=  CsTaxRate;
    end;
    //2014.04.01 消費税対応 end

    curShouhizei      := Round(StrtoCurr(strShoukei) * douTax + 0.01);
    curGoukei         := StrtoCurr(strShoukei) + curShouhizei;
    strShouhizei      := CurrtoStr(curShouhizei);
    strGoukei         := CurrtoStr(curGoukei);

    strNouhinDate     := strDdate;
    strDenpyouCode    := strDenpyouBangou;

  try
    // TokuisakiCode2の取得
    sSql := 'SELECT TokuisakiCode2 FROM ' + CtblMTokuisaki;
    sSql := sSql + ' WHERE TokuisakiCode1=' + strTokuisakiCode1;
    with QueryDenpyou do begin
	 	  Close;
      Sql.Clear;
    	Sql.Add(sSql);
      Open;
      strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
      Close;
    end;

    //DELETE FROM CtblTDenpyou , CtblTDenpyouDetail, CtblTGennkinnKaishuu WHEN strInfoDenpyouNo IS EXISTS the same over 1 place in csv files
    sSql := 'DELETE FROM ' + CtblTDenpyou;
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + strInfoDenpyouNo + '''';
    sSql := sSql + ' AND TokuisakiCode1 = ' + '''' + strTokuisakiCode1 + '''';


    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;

    sSql := 'DELETE FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + strInfoDenpyouNo + '''';
    sSql := sSql + ' AND TokuisakiCode1 = ' + '''' + strTokuisakiCode1 + '''';

    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;

    sSql := 'DELETE FROM ' + CtblTGennkinnKaishuu;
    sSql := sSql + ' WHERE InfoDenpyouNo = ' + '''' + strInfoDenpyouNo + '''';
    sSql := sSql + ' AND TokuisakiCode1 = ' + '''' + strTokuisakiCode1 + '''';

    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;


    //NSERT TO CtblTDenpyou
    // SQL文作成
    sSql := 'INSERT INTO ' + CtblTDenpyou;
    sSql := sSql + ' ( TokuisakiCode1, TokuisakiCode2, InputDate, DDate, ';
    sSql := sSql + 'UriageKingaku, Gatsubun, DenpyouCode, Memo, Teiseiflg, Bikou, InfoDenpyouNo )';
    sSql := sSql + ' VALUES ( ';
    sSql := sSql + '''' + strTokuisakiCode1  + ''', ' ;
    sSql := sSql + '''' + strTokuisakiCode2  + ''', ' ;
    sSql := sSql + '''' + strInputDate       + ''', ' ;
    sSql := sSql + '''' + strDDate           + ''', ' ;
    sSql := sSql +        strUriageKingaku   + ', '   ;
    sSql := sSql + '''' + strGatsubun        + ''', ' ;
    sSql := sSql + '''' + strDenpyouCode     + ''', ' ;
    sSql := sSql + '''' + strInfoDenpyouNo   + ''', ' ;
    sSql := sSql + '''' + strTeiseiflg       + ''', ' ;
    sSql := sSql + '''' + strBikou           + ''', ' ;
    sSql := sSql + '''' + strInfoDenpyouNo   + ''' )' ;

    with QueryDenpyou do begin
  	  Close;
      Sql.Clear;
    	Sql.Add(sSql);
   	  ExecSql;

      Close;
    end;

if (StrToInt(strTokuisakiCode1) >= 50000) and (StrToInt(strTokuisakiCode1) < 60000) then begin

   // tblTGennkinnkaishuuへデータを挿入

   // SQL文作成
   sSql := 'INSERT INTO ' + CtblTGennkinnKaishuu;
   sSql := sSql + ' ( DenpyouCode, NouhinDate, TokuisakiCode1, Shoukei, Shouhizei, Goukei, CourseID, Kaishuuflg, InfoDenpyouNo)';
   sSql := sSql + ' VALUES ( ';
   sSql := sSql + '''' + strDenpyouCode     + ''', ' ;
   sSql := sSql + '''' + strDDate           + ''', ' ;
   sSql := sSql + '''' + strTokuisakiCode1  + ''', ' ;
   sSql := sSql +        strUriageKingaku   + ', '   ;
   sSql := sSql +        strShouhizei       + ', '   ;
   sSql := sSql +        strGoukei          + ', '   ;
   sSql := sSql + '''' + strTokuisakiCode2  + ''', ' ;
   sSql := sSql + '''' + '0'  + ''', ' ;
   sSql := sSql + '''' + strInfoDenpyouNo   + ''' )' ;

   // SQL文実行
   with QueryDenpyou do begin
   	 Close;
     Sql.Clear;	
   	 Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;
end;
  Result := True;
  except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  ShowMessage(E.Message);
      Result := False;
    end;
  end;
end;

Function TfrmInfoMart.InsertDenpyouDetail(DenpyouDetail:TStringList; strDenpyouBangou:String) : Boolean;
  var sSql          : String;
  strCode           : TStringList;

  strTokuisakiCode1 : String;
  strNyuuryokusha   : String;
  strNo             : String;
  strCode1          : String;
  strCode2          : String;
  strSuuryou        : String;
  strTannka         : String;
  strShoukei        : String;
  strNouhinDate     : String;
  strNyuuryokuDate  : String;
  strDennpyouDate   : String;
  strUpdateDate     : String;
  strInfoDenpyouNo  : String;
  strGenka          : String;  

begin

    strTokuisakiCode1 := DenpyouDetail[7];     //取引先コード
    strCode := TStringList.Create;


    strNyuuryokusha   := CBNyuuryokusha.text;
    strNo             := DenpyouDetail[14];    //伝票明細番号

    strCode1 :='';
    strCOde2 :='';
    if DenpyouDetail[16] <> '' then begin
        Split('-', DenpyouDetail[16], strCode);    //自社管理商品コード
        strCode1          := strCode[0];
        if(strCode.Count > 1) then begin
          strCode2          := strCode[1];
        end;
    end;

    strSuuryou        := DenpyouDetail[22];    //数量


    //mod before 20190313 by ytada
    {strTannka         := DenpyouDetail[21];    //単価
    strShoukei        := DenpyouDetail[24];    //金額
    }
    //add 20190313 得意先マスタから取得する
    strTannka         := GetTanka(strCode1,strCode2,strTokuisakiCode1);
    
    //add 20190313 by utada 単価と数量から計算する
    strShoukei        := FloatToStr(StrToFloat(strTannka) * StrToFloat(strSuuryou));


    //strNouhinDate     := DenpyouDetail[37];  mod before
    strNouhinDate     :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada

    DateTimeToString(strNyuuryokuDate, 'yyyy/mm/dd', now);
    //strDennpyouDate   := DenpyouDetail[37];    //納品日     mod before
    strDennpyouDate     :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada

    DateTimeToString(strUpdateDate, 'yyyy/mm/dd', now);
    strInfoDenpyouNo  := trim(DenpyouDetail[2]);//伝票No
    strGenka      := GetGenka(strCode1,strCode2);

  try
	  sSql := 'INSERT INTO ' + CtblTDenpyouDetail + ' (';
  	sSql := sSql + ' DenpyouCode, TokuisakiCode1, Nyuuryokusha,';
  	sSql := sSql + ' No, Code1, Code2, Suuryou, Tannka, Shoukei,';
  	sSql := sSql + ' NouhinDate, NyuuryokuDate, DenpyouDate, UpdateDate, Genka, InfoDenpyouNo)';
  	sSql := sSql + ' VALUES ( ';
  	sSql := sSql + '''' + strDenpyouBangou   + ''', ' ;
  	sSql := sSql +        strTokuisakiCode1  +   ', ' ;
  	sSql := sSql + '''' + strNyuuryokusha    + ''', ' ;
  	sSql := sSql + '''' + strNo              + ''', ' ;
  	sSql := sSql + '''' + strCode1           + ''', ' ;
  	sSql := sSql + '''' + strCode2           + ''', ' ;
  	sSql := sSql + '''' + strSuuryou         + ''', ' ;
  	sSql := sSql + '''' + strTannka          + ''', ' ;
  	sSql := sSql + '''' + strShoukei         + ''', ' ;
  	sSql := sSql + '''' + strNouhinDate      + ''', ' ;
  	sSql := sSql + '''' + strNyuuryokuDate   + ''', ' ;
  	sSql := sSql + '''' + strDennpyouDate    + ''', ' ;
  	sSql := sSql + '''' + strUpdateDate      + ''', ' ;
    sSql := sSql + '''' + strGenka           + ''', ' ;
  	sSql := sSql + '''' + strInfoDenpyouNo   + ''' )' ;
    
    with QueryDenpyou do begin
  		 	  Close;
	        Sql.Clear;
  	  	  Sql.Add(sSql);
  		    ExecSql;
	        Close;
    end;
    Result := True;    
  except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  ShowMessage(E.Message);
      Result := False;
    end;
  end;
end;

function TfrmInfoMart.Replace(Str, Before, After: string): string;
var
  Len, N: Integer;
begin
  Len := Length(Before) - 1;
  N := AnsiPos(Before, Str);
  while N > 0 do begin
    Result := Result + Copy(Str, 1, N - 1) + After;
    Delete(Str, 1, N + Len);
    N := AnsiPos(Before, Str)
  end;
  Result := Result + Str
end;


Function TfrmInfoMart.DeleteAll(strInfoDenpyouNo:String): Boolean;
    var sSql              : String;
begin
    try
    //DELETE FROM CtblTDenpyou , CtblTDenpyouDetail, CtblTGennkinnKaishuu WHEN strInfoDenpyouNo IS EXISTS the same over 1 place in csv files
    sSql := 'DELETE FROM ' + CtblTDenpyou;
    sSql := sSql + ' WHERE InfoDenpyouNo IN (' + strInfoDenpyouNo + ')';
    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;

    sSql := 'DELETE FROM ' + CtblTDenpyouDetail;
    sSql := sSql + ' WHERE InfoDenpyouNo IN (' + strInfoDenpyouNo + ')';
    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;

    sSql := 'DELETE FROM ' + CtblTGennkinnKaishuu;
    sSql := sSql + ' WHERE InfoDenpyouNo IN (' + strInfoDenpyouNo + ')';
    with QueryDenpyou do begin
	 	    Close;
        Sql.Clear;
  	    Sql.Add(sSql);
    	  ExecSql;
        Close;
    end;


  Result := True;
  except
    on E: EDBEngineError do begin
      dabDenpyou.Rollback;
   	  //ShowMessage(E.Message);
      Result := False;
    end;
  end;

end;

Function TfrmInfoMart.UpdateItem(DenpyouDetail:TStringList) : Boolean;

var
 i                 : Integer;
 iHindo            : Integer;
 iTanka            : Integer;
 strTokuisaki      : String;
 strCode1          : String;
 strCode2          : String;
 strSaishuu        : String;
 sSql,strTokuisakiCode1 : String;
 strDDate          : String;
 strTanka          :String;
 iDTanka           :Integer;
 sDate             :String;
 strCode           :TStringList;
 iBikouLen         : Integer;
begin

   strCode := TStringList.Create;

   strCode1 :='';
   strCode2 :='';
   if (DenpyouDetail[16] <> '' ) then begin
      Split('-', DenpyouDetail[16], strCode);    //自社管理商品コード
      // 頻度を取得
      strCode1             := strCode[0];
      if(strCode.Count > 1) then begin
          strCode2          := strCode[1];
      end;
   end;

  //strDdate             := DenpyouDetail[37];  //納品日  mod before
   strDdate             :=DatetoStr(DTP1.Date);  //納品日 mod after 20130227 utada
   //strTanka             := DenpyouDetail[21]; //単価
   //iDTanka              := Trunc(StrToFloat(strTanka));

try
   sSql := 'SELECT Hindo,Tanka FROM ' + CtblMItem2;
   sSql := sSql + ' WHERE Code1 = ' + '''' + strCode1 + '''';
   sSql := sSql + ' AND   Code2 = ' + '''' + strCode2 + '''';

   strTokuisakiCode1    := DenpyouDetail[7];     //取引先コード
   sSql := sSql + ' AND   TokuisakiCode = ' + '''' + strTokuisakiCode1 + '''';


   with QueryDenpyou do begin
	   Close;
     Sql.Clear;
  	 Sql.Add(sSql);
     Open;
     iHindo := FieldbyName('Hindo').AsInteger;
     iTanka := FieldbyName('Tanka').AsInteger;
     Close;
   end;


   // 頻度，最終出荷日を更新
   iHindo       := iHindo + 1;
   strTokuisaki         := DenpyouDetail[7];     //取引先コード
   strSaishuu := strDDate;  //Add utada 2007.08.24 最終出荷日は、納品日に合わせる
   sSql := 'UPDATE ' + CtblMItem2;
   sSql := sSql + ' SET Hindo = '           + InttoStr(iHindo)    + ', ';
   sSql := sSql + ' SaishuuShukkabi = '     + '''' + strSaishuu   + '''';
   sSql := sSql + ' WHERE TokuisakiCode = ' + '''' + strTokuisaki + '''';
   sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
   sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;

   //Button BitBtn8Click Click

  {
   if IntToStr(iTanka) <> IntToStr(iDTanka) then begin

      sSql := 'UPDATE ' + CtblMItem2;
      sSql := sSql + ' SET Tanka = ' + IntToStr(iDTanka);
      sSql := sSql + ' WHERE TokuisakiCode = ' + '''' + strTokuisaki + '''';
      sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
      sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

      with QueryDenpyou do begin
        Close;
        Sql.Clear;
        Sql.Add(sSql);
 	      ExecSql;
        Close;
      end;

   end;
   }

   //sDate := DateToStr(Date);

   {
   //見積りデータの更新
   sSql := 'SELECT Tanka,DATALENGTH(CONVERT(VARCHAR(255), bikou)) as bikou FROM ' + CtblMItem3;
   sSql := sSql + ' WHERE Code1 = ' + '''' + strCode1 + '''';
   sSql := sSql + ' AND   Code2 = ' + '''' + strCode2 + '''';

   strTokuisakiCode1    := DenpyouDetail[7];     //取引先コード
   sSql := sSql + ' AND   TokuisakiCode = ' + '''' + strTokuisakiCode1 + '''';


   with QueryDenpyou do begin
        Close;
        Sql.Clear;
        Sql.Add(sSql);
        Open;
        iBikouLen := FieldbyName('bikou').AsInteger;
        iTanka := FieldbyName('Tanka').AsInteger;
        Close;
   end;

   if IntToStr(iTanka) <> strTanka then begin
      sSql := 'UPDATE tblMItem3';
      sSql := sSql + ' SET Tanka = ' + '''' + strTanka   + '''';

      //備考欄のMaxLenghtが255までのため

      if(iBikouLen > 246) then begin
         // sSql := sSql + ', Bikou = ''' + sDate + '価格変更''';
      end else begin
        //  sSql := sSql + ', Bikou = ''' + sDate + '価格変更, '' + Bikou';
      end;

      sSql := sSql + ' WHERE TokuisakiCode = ' + '''' + strTokuisaki + '''';
      sSql := sSql + ' AND   Code1 = '         + '''' + strCode1     + '''';
      sSql := sSql + ' AND   Code2 = '         + '''' + strCode2     + '''';

      with QueryDenpyou do begin
         Close;
         Sql.Clear;
         Sql.Add(sSql);
         ExecSql;
         Close;
      end;
  end;    //End

  }


  Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;

Function TfrmInfoMart.GetDenpyouBanngou(strTokuisakiCode1:String; strGDate: String) : String;
var
 sSql              : String;	
 wkGDenpyouBangou  : String;	
 wkDate1           : String;	
 wkDate2           : String;
 wksNumber         : String;	
 sYy,sYy2,sYyNext  : String;	
 sAriaCode         : String;
 GDate             : String;
 strDenpyouBangou  : String;




begin
	
try
  strDenpyouBangou :='';

  // もしGDenpyouBangouが既にセットされているなら，(Denpyou1より引き継いでいるなら）	
  // それは既存伝票の修正なので，それをそのまま使う．よって，この関数を抜ける．	
  // GDenpyouBangouがnullの場合，新規伝票なので伝票番号生成へ移行する．	
  //if gDenpyouNo <> '' then begin
  //  Result := True;
  //  Exit;
  //end;	
	

  // 伝票番号生成開始	
	
   //2004.09.18
   // 伝票番号の上4桁（実日付 YYMM)を取得する．	
   //DateTimeToString(wkDate1, 'yymm', Date());	
   // 伝票番号の上2桁（実日付 YY)を取得する．	
   DateTimeToString(wkDate1, 'YY', Date());	
   sAriaCode:=GetAriaCode(strTokuisakiCode1);	
   //2005.01.12	
   //sYYのとり方を変更	
   //理由：昨年の伝票を打つときに伝票番号が正しくとれない為	
   //伝票日付の年をとるようにする
   //DateTimeToString(sYy, 'YY', Date());	
   GDate:=strGDate;
   sYy := Copy(GDate,3,2);	
	
   wkDate1 := wkDate1+ sAriaCode;
   //一応エラーチェック	
   if Length(wkDate1) <> 4 then begin
    ShowMessage('伝票番号の上4桁が不正です->' + wkDate1);
    exit;
   end;
	
   // 伝票番号の最大値を取得し，その下5桁を取得する．	
   //	
   // 1.伝票自動採番ロジックの変更	
   //	
   //2004.09.18	
   //04　11　　　　　　00001	
   //年　コースコード　連番	
   //	
   //エリアコードも考慮してマックス値をとる	
   //sSql := 'SELECT MAX(DenpyouCode) FROM ' + CtblTDenpyouDetail;	
   //	
   //2004.10.17	
   //伝票番号作成時の年号も考慮しないといけない	
   // sYy	
   //
   //	
   sSql := 'SELECT MAX(DenpyouCode) FROM ' + CtblTDenpyou;	
   sSql := sSql + ' where ';	
   sSql := sSql + ' DDate >= ''20' + sYy + '/01/01''';	
   sSql := sSql + ' and ';	

   //2005.01.05
   //年度単位で伝票番号のMAXを求めるため	
   sYyNext := IntToStr(StrToInt(sYy)+1);	
   If Length(sYyNext)=1 then begin
    sYyNext := '0' + sYyNext;
   end;	
   sSql := sSql + ' DDate < ''20' + sYyNext + '/01/01''';
   sSql := sSql + ' and ';	

	
   sSql := sSql + ' TokuisakiCode2 = ' + sAriaCode;

   with QueryDenpyou do begin	
      QueryDenpyou.	
   	 Close;
     Sql.Clear;	
     Sql.Add(sSql);	
     Open;	
     if EOF then begin	
      wkGDenpyouBangou := '000000000';	
     end else begin	
      wkGDenpyouBangou := Fields.FieldByNumber(1).AsString;	
     end;	
     Close;	
   end;	
	
   //2005.12.29	
   if wkGDenpyouBangou='' then begin //年初の場合	
     strDenpyouBangou := sYy + sAriaCode + '00001';
   end else begin	
    sYy2      := Copy(wkGDenpyouBangou, 1, 2);	
    wkDate2   := Copy(wkGDenpyouBangou, 1, 4);	
    wksNumber := Copy(wkGDenpyouBangou, 5, 9);	
	
    // 伝票番号の下4桁の生成	
	
    // もし月初の1番最初の伝票であるなら，伝票番号下5桁を00001にセットする．	
    // この時のロジックは以下の通り．	
    //　 実日付から生成した伝票番号の上4桁(wkDate1)と，	
    //　 現在の最後の伝票番号の上4桁が異なる．	
    //　 例えば，wkDate1='0112' wkDate2='0111'であれば月初の最初の伝票．	
    //　 月初の2個目の伝票以降は，wkDate1='0112' wkDate2='0112'となる．	
    //　 これは月初営業日が1日で無い場合にも対応する．	
    //if wkDate1 <> wkDate2 then	
    if sYy <> sYy2 then begin	
      wksNumber := '00001';	
    end else begin	
      // 月初の一番最初の伝票で無い場合は，単純にプラス1する．	
      wksNumber   := FormatFloat('00000', (StrToInt(wksNumber) + 1));	
	
      //2004.09.18	
      //一応エラーチェック
      if StrToint(wksNumber) > 90000 then begin	
        Showmessage('警告！　エリアコード->' + wkDate1 + '連番が90000を超えました');	
        Showmessage('処理は続行できますが一応管理者へ電話してください');	
      end;	
    end;	
	
     // 伝票番号の生成	
    //2005.01.12	
    //wkDate1は現時点の日付を保持しているため
    //gDenpyouNo := wkDate1 + wksNumber;
    strDenpyouBangou := wkDate2 + wksNumber;
   end; //ofif 2005.12.29
   Result := strDenpyouBangou;
except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
    Result := '';
  end;
end;	
	
end;	
	
Function  TfrmInfoMart.GetAriaCode(sTokuisakiCode1:String) : String;
var	
  sSql,sTokuisakiCode2 : String;	
begin	
  sSql := 'Select TokuisakiCode2 from ' + CtblMTokuisaki;
  sSql := sSql + ' where ';
  sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with Query2 do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then begin	
      ShowMessage('この得意先に登録されているエリアコードがありません');
      Close;
      Exit;	
    end else begin
      sTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
      Close;	
    end;
    Result := sTokuisakiCode2;
  end;//of with
end;


Function  TfrmInfoMart.GetGenka(sCode1,sCode2:String) : String;
var
  sSql : String;
  sGenka : String;
begin
  sSql := 'Select Genka from ' + CtblMItem;
  sSql := sSql + ' where ';
  sSql := sSql + ' Code1 = ' + '''' + sCode1 + '''';
  sSql := sSql + ' AND Code2 = ' + '''' + sCode2 + '''';
  with Query2 do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then begin
       sGenka := '0';
    end else begin
      sGenka := FieldbyName('Genka').AsString;
      Close;
    end;
    Result := sGenka;
  end;//of with
end;

//add 20190316 by utada  得意先マスタの価格優先対応
Function  TfrmInfoMart.GetTanka(sCode1,sCode2,sTokuisakiCode:String) : String;
var
  sSql : String;
  sTanka : String;
begin
  sSql := 'Select Tanka from ' + CtblMItem2;
  sSql := sSql + ' where ';
  sSql := sSql + ' Code1 = ' + '''' + sCode1 + '''';
  sSql := sSql + ' AND Code2 = ' + '''' + sCode2 + '''';
  sSql := sSql + ' AND TokuisakiCode = ' + '''' + sTokuisakiCode + '''';

  with QueryDenpyou do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then begin
       sTanka := '0';
       Close;
    end else begin
      sTanka := FieldbyName('Tanka').AsString;
      Close;
    end;
    Result := sTanka;
  end;//of with
end;


//add 20190316 by utada  得意先マスタの価格優先対応
Function TfrmInfoMart.UpdateDenpyou(strDenpyouBangou:String) : Boolean;

var
 sSql,sSql2 : String;

begin

try
   //合計金額の再計算
   sSql2:= 'SELECT SUM(Shoukei) FROM tblTDenpyouDetail WHERE DenpyouCode = ' + '''' + strDenpyouBangou + '''';
   sSql := 'UPDATE ' + CtblTDenpyou;
   sSql := sSql + ' SET UriageKingaku = (' + sSql2 + ')';
   sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBangou + '''';

   with QueryDenpyou do begin
     Close;
     Sql.Clear;
     Sql.Add(sSql);
 	   ExecSql;
     Close;
   end;
  Result := True;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;
//add 20190316 by utada  得意先マスタの価格優先対応
Function TfrmInfoMart.UpdateGenkinDenpyou(strDenpyouBangou:String) : Boolean;

var
 sSql : String;
 
 strShoukei        : String;
 strShouhizei      : String;
 strGoukei         : String;
 strTokuisakiCode1 : String;
 curShouhizei      : Currency;
 curGoukei         : Currency;
 douTax            : Double;

begin

try

    douTax :=  CsTaxRate;

    sSql := 'SELECT SUM(Shoukei) as Shoukei,TokuisakiCode1 FROM tblTDenpyouDetail WHERE DenpyouCode = ' + '''' + strDenpyouBangou + '''';
    sSql := sSql + ' Group By TokuisakiCode1';

   with QueryDenpyou do begin
   	  Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
      if RecordCount =0 then begin
         Close;
         Exit;
      end else begin
         strShoukei := FieldbyName('Shoukei').AsString;
         strTokuisakiCode1 := FieldbyName('TokuisakiCode1').AsString;
         Close;
      end;

    end;//of with

    curShouhizei      := Round(StrtoCurr(strShoukei) * douTax + 0.01);
    curGoukei         := StrtoCurr(strShoukei) + curShouhizei;
    strShouhizei      := CurrtoStr(curShouhizei);
    strGoukei         := CurrtoStr(curGoukei);


   //合計金額の再計算

   if (StrToInt(strTokuisakiCode1) >= 50000) and (StrToInt(strTokuisakiCode1) < 60000) then begin


	   sSql := 'UPDATE ' + CtblTGennkinnKaishuu;
	   sSql := sSql + ' SET Shoukei = ' + strShoukei;
	   sSql := sSql + ' ,Shouhizei = ' + strShouhizei;
	   sSql := sSql + ' ,Goukei = ' + strGoukei;

	   sSql := sSql + ' WHERE DenpyouCode = ' + '''' + strDenpyouBangou + '''';

	   with QueryDenpyou do begin
	     Close;
	     Sql.Clear;
	     Sql.Add(sSql);
	 	   ExecSql;
	     Close;
	   end;
	   Result := True;
   end;

except
  on E: EDBEngineError do begin
    dabDenpyou.Rollback;
   	ShowMessage(E.Message);
    Result := False;
  end;
end;

end;


end.
