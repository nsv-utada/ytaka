unit KakakuKoushin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, Db, DBTables, ShellAPI,
  DBXpress, SqlExpr;

type
  TfrmKakakuKoushin = class(TForm)
    SG1: TStringGrid;
    SB2: TStatusBar;
    Query1: TQuery;
    dabMitsumori: TDatabase;
    pnlInput: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label2: TLabel;
    Label1: TLabel;
    Label19: TLabel;
    CBCode1: TComboBox;
    CBName: TComboBox;
    Label20: TLabel;
    Label21: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Memo: TMemo;
    Button2: TButton;
    cmbCourse: TComboBox;
    CBYomi: TComboBox;
    Label32: TLabel;
    CBCode2: TComboBox;
    LabelSID: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    tanka420: TLabel;
    CBYyyy: TComboBox;
    CBMm: TComboBox;
    CBYyyyTo: TComboBox;
    CBMmTo: TComboBox;
    Button1: TButton;
    Label6: TLabel;
    Label7: TLabel;
    tanka450: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EditChainCode: TEdit;
    Label13: TLabel;
    GroupBox2: TGroupBox;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    GroupBox1: TGroupBox;
    
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);

	//event on Combox
	procedure CBCode1Exit(Sender: TObject);
	procedure CBCode1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	procedure CBCode2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);  
	procedure CBNameExit(Sender: TObject);
	procedure CBNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);	
	procedure CBYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);	
		
    procedure Button1Click(Sender: TObject);
    
	procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SG1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	procedure BitBtn1Click(Sender: TObject);
    function GetCourseID(CourseName: String) : String;
    procedure SG1DblClick(Sender: TObject);
    procedure SG1KeyPress(Sender: TObject; var Key: Char);
    procedure MemoKeyPress(Sender: TObject; var Key: Char);
    procedure SG1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
  private
    procedure MakeSG;
    procedure MakeSG2;
	
	procedure MakeCBName(sKey, sWord:String; iKubun:Integer);
	 procedure MakeForm(sid:integer);
	 function GetItemCode2:string;
    
    Procedure GridSort(Grid : TStringGrid;SortCol : LongInt);
    Procedure GridClear;

  public
    { Public declarations }
  end;
  const
	//グリッドの列の定義  　　	

  CintItemCheck       = 0;
  CintItemCode1       = 1;
  CintItemName        = 2;
  CintItemHindo       = 3;
  CintItemSaishuuShukkabi = 4;
  CintItemTanka       = 5;
  CintItemPrice       = 6;
  CintItemSID         = 7;
  CintEnd      		    = 8;
  
  CStrChkValue = '   1';
var
  frmKakakuKoushin: TfrmKakakuKoushin;
  
implementation
uses
	DMMaster, inter, HKLib,PasswordDlg;

var
	gSort  : Integer;
{$R *.DFM}

procedure TfrmKakakuKoushin.FormCreate(Sender: TObject);
var sSql, CurYear, CurMonth : String;
begin
  //inherited;
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
 if dlgPasswordDlg.ShowModal = mrOK then begin

 end else begin
 		Exit;
 end;


  top  := 5;
  Left := 15;
  Width := 800;
  Height := 700;
  Memo.Text := '';

  CurYear := FormatDateTime('yyyy', Now);
  CurMonth := FormatDateTime('mm', Now);
  CBYyyy.Text := CurYear;
  CBMm.Text :=  CurMonth;
  CBYyyyTo.Text := CurYear;
  CBMmTo.Text :=  CurMonth;
  
  //
  cmbCourse.Items.Clear;
  
  try

  // コンボボックスリスト作成
	sSql := 'SELECT CourseName FROM ' + CtblMCourse;
  sSql := sSql + ' order by CourseID ';

  cmbCourse.Items.Add('');
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	cmbCourse.Items.Add(FieldByName('CourseName').AsString);
    	Next;
    end;
    Close;
  end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;
  
  //ストリンググリッドの作成
  MakeSG;
end;

procedure TfrmKakakuKoushin.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    //RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
	
    ColWidths[CintItemCheck]:= 45;
    Cells[CintItemCheck, 0] := 'ﾁｪｯｸ';
	
	  ColWidths[CintItemCode1]:= 100;
    Cells[CintItemCode1, 0] := '得意先コード■';
	
  	ColWidths[CintItemName]:= 260;
    Cells[CintItemName, 0] := '得意先名';

	  ColWidths[CintItemHindo]:= 40;
    Cells[CintItemHindo, 0] := '頻度';
	
	  ColWidths[CintItemSaishuuShukkabi]:= 100;
    Cells[CintItemSaishuuShukkabi, 0] := '最終出荷日';

	  ColWidths[CintItemTanka]:= 100;
    Cells[CintItemTanka, 0] := '現在価格';

   	ColWidths[CintItemPrice]:= 100;
    Cells[CintItemPrice, 0] := '変更後価格';

    ColWidths[CintItemSID]:= 0;
    Cells[CintItemSID, 0] := '';


  end;
end;

procedure TfrmKakakuKoushin.MakeSG2;
var
	 sSql            : String;
  dbCode1			    : String;
  dbCode2	      		: String;
  dbCourse	      		: String;
  sDateFrom				: String;
  sDateTo				: String;

  i                		: Integer;
  j                		: Integer;
  gRowNum          		: Integer;
begin

  dbCode1 := CBCode1.Text;
  dbCode2 := CBCode2.Text;
  dbCourse := GetCourseID(cmbCourse.Text);
  sDateFrom := CBYyyy.Text + '/' + CBMm.Text + '/01';
  //sDateTo := CBYyyyTo.Text + '/' + CBMmTo.Text + '/31';
  sDateTo := CBYyyyTo.Text + '/' + CBMmTo.Text + '/' + IntToStr(HKLib.GetGetsumatsu(StrToInt(CBYyyyTo.Text), strToInt(CBMmTo.Text)));

  // Make SQL
 	sSql := 'SELECT * FROM ' + CtblMItem2;
    sSql := sSql + ' I LEFT JOIN ' + CtblMTokuisaki + ' T ON';
    sSql := sSql + ' I.tokuisakicode = T.TokuisakiCode1 ';
    sSql := sSql + ' WHERE ';
  	sSql := sSql + ' I.Code1 = ''' + dbCode1 + '''';
	  sSql := sSql + ' AND ';
	  sSql := sSql + ' I.Code2 = ''' + dbCode2 + '''';
	  sSql := sSql + ' AND ';

    if(EditChainCode.text <> '') then begin

	    sSql := sSql + ' T.ChainCode = ''' + EditChainCode.text + '''';

    end else begin

      sSql := sSql + ' I.SaishuuShukkabi BETWEEN ''' + sDateFrom + '''';
      sSql := sSql + ' AND ';
      sSql := sSql + '''' + sDateTo + '''';
      if(dbCourse <> '') then begin
  	    sSql := sSql + ' AND ';
	      sSql := sSql + ' T.TokuisakiCode2 = ''' + dbCourse + '''';
      end;
      sSql := sSql + ' AND ';
      sSql := sSql + ' I.Hindo > 0';

    end ;

    sSql := sSql + ' Order By I.Tokuisakicode';

  // Excecute SQL
  with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      FetchAll;

      // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
      if RecordCount <= 0 then
        begin
          SG1.RowCount := 2;
          MakeSG;
          ShowMessage('この得意先に登録されている商品がありません');
          Close;
          Exit;
        end;

      // Set RowCount
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;

      // Set Data to StringGrid
      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
          Cells[CintItemCheck,   i] := '0';
          Objects[CintItemCheck, i] := TObject(2);  //CheckBoxの表示
      	  Cells[CintItemCode1,   i] := FieldbyName('TokuisakiCode1').AsString;
      	  Cells[CintItemName,   i] := FieldbyName('TokuisakiName').AsString;
      	  Cells[CintItemHindo,   i] := FieldbyName('Hindo').AsString;
      	  Cells[CintItemSaishuuShukkabi, i] := FieldbyName('SaishuuShukkabi').AsString;
          Cells[CintItemTanka,   i] := FieldbyName('Tanka').AsString;
          Cells[CintItemPrice,    i] := '';
          Cells[CintItemSID,    i] := FieldbyName('SID').AsString;
        end;
        next
      end;
      Close;
    end;
end;

procedure TfrmKakakuKoushin.FormResize(Sender: TObject);
begin
  SG1.Align := alClient;
end;

procedure TfrmKakakuKoushin.BitBtn1Click(Sender: TObject);
begin
	Close;
end;


procedure TfrmKakakuKoushin.FormActivate(Sender: TObject);
begin
CBCode1.Items.LoadFromFile('code1.txt');
  //
	if GPassWord = CPassword0 then begin
    Beep;
  end else begin
  	//ShowMessage('PassWordが違います');
    close;
  end;
  GPassWord := '';


end;

Procedure TfrmKakakuKoushin.GridClear;
var i,j : Integer;

begin

  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
       if j = i then begin
         SG1.Objects[CintItemCheck, j] := TObject(3);
       end;
      end;
    end;
  end;

end;

procedure TfrmKakakuKoushin.Button1Click(Sender: TObject);
begin
  GridClear;
  MakeSG2;
end;

procedure TfrmKakakuKoushin.SG1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
ACol, ARow: Integer;
SortMark : String;
begin

  SG1.MouseToCell(X, Y, ACol, ARow);


  if (ACol = CintItemCheck) And (ARow > 0) then
  begin

    if SG1.Cells[CintItemCheck, ARow] = '0' then  begin
       SG1.Cells[CintItemCheck, ARow] := CStrChkValue;
       //SG1.Objects[CintItemCheck, ARow] := TObject(1);

    end else begin
       SG1.Cells[CintItemCheck, ARow] := '0';
       //SG1.Objects[CintItemCheck, ARow] := TObject(2);
    end;


    SG1.Row := ARow;
    SG1.Col := CintItemCode1;
    SG1.SetFocus;
    
  end;


    if (ACol = CintItemCheck) or (ACol = CintItemName)  or (ACol = CintItemTanka)  or (ACol = CintItemPrice)  then  exit;


    if (ARow = 0) and (SG1.Cells[CintItemCode1, 1] <> '') then
    begin

      GridSort(SG1, ACol);


      if gSort = 1 then begin
        SortMark := '▲';
      end else begin
        SortMark := '▼';
      end;

      MakeSG;
	  
      SG1.Cells[ACol, 0] := Copy(SG1.Cells[ACol, 0],0,Length(SG1.Cells[ACol, 0]) -2);
      SG1.Cells[ACol, 0] := SG1.Cells[ACol, 0] + SortMark;

    end;

end;

procedure TfrmKakakuKoushin.SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
type
  TVowels = set of char;  
var
 myRect : TRect;
 iCol : Integer;
  SelectionSetRect: TGridRect;
  DRect : TRect;
  DArari : Double;
begin
    if SG1.RowHeights[ARow] = -1 then exit;

 	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;
	
   SG1.Canvas.FillRect(Rect);
  	DrawText(SG1.Canvas.Handle, PChar(SG1.Cells[ACol,ARow]),
	           Length(SG1.Cells[ACol,ARow]), DRect, DT_CENTER);

    if (ACol = 0) and (ARow > 0)  then begin
      if (SG1.Cells[CintItemCheck,ARow] = CStrChkValue) then begin
          DrawFrameControl(TStringGrid(Sender).Canvas.Handle,
          Rect, DFC_BUTTON, DFCS_CHECKED);
      end else begin
          DrawFrameControl(TStringGrid(Sender).Canvas.Handle,
          Rect, DFC_BUTTON, DFCS_BUTTONCHECK);
      end;

    end;

	  for iCol := CintItemName to CintItemName do begin
    		myRect := SG1.CellRect(iCol, ARow);
	      SG1.Canvas.FillRect(myRect);

		    DrawText(SG1.Canvas.Handle,
  	    					PChar(SG1.Cells[iCol, ARow]),
	  		          Length(SG1.Cells[iCol, ARow]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
    end;//of for

    for iCol := CintItemTanka to CintItemPrice do begin
       if (ARow > 0) then begin
    		myRect := SG1.CellRect(iCol, ARow);
	      SG1.Canvas.FillRect(myRect);

		    DrawText(SG1.Canvas.Handle,
  	    					PChar(SG1.Cells[iCol, ARow]),
	  		          Length(SG1.Cells[iCol, ARow]),
        		      myRect,
            	    DT_RIGHT);
        end;
    end;//of for

end;

procedure TfrmKakakuKoushin.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

//event on Combox
procedure TfrmKakakuKoushin.CBCode1Exit(Sender: TObject);
begin
  CBCode2.Text := GetItemCode2();
end;

function TfrmKakakuKoushin.GetItemCode2:String;
var
  sItemCode1,sWhere : String;
  i : Integer;
begin
  if (Pos(',',CBCode1.Text) > 1) then begin
    sItemCode1 := Copy(CBCode1.Text, 1, Pos(',',CBCode1.Text)-1);
    sWhere := ' Code1=''' +sItemCode1 + '''';
    CBCode2.Text := DMMaster.GetFieldData(CtblMItem, 'cm=max(Code2)', sWhere);
    if CBCode2.Text = '' then begin
      CBCode2.Text := '0';
    end;

    i := StrToInt(CBCode2.Text)+1;
    //例：FixLength('123', '0', 8)
    //			結果：'00000123'
    result := HKLib.FixLength(IntToStr(i), '0', 4);
  end;//
end;

procedure TfrmKakakuKoushin.CBCode1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBName('Code1', CBCode1.Text, 0);
  end;
end;

procedure TfrmKakakuKoushin.CBCode2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBName('Code2', CBCode2.Text, 0);
  end;
end;

procedure TfrmKakakuKoushin.CBNameExit(Sender: TObject);
var
  sid : integer;
  sItemName : string;
begin
  if CBName.Text = '' then begin
    exit;
  end;
	//SIDを取得して検索する
  if (pos(',', CBName.Text)>0) then begin
    sid := StrToInt(Copy(CBName.Text, 1, (pos(',', CBName.Text)-1)));
    sItemName := Copy(
                     CBName.Text,
                     (pos(',', CBName.Text)+1),
                     Length(CBName.Text)
                    );
    if (pos(',',sItemName)>1) then begin
      sItemName := Copy(sItemName, 1, pos(',',sItemName)-1);
    end;
    CBName.Text :=sItemName;
  end else begin
     if (LabelSID.caption <> '' ) then begin
      sid := StrToInt(LabelSID.caption);
     end else begin
      sid := 0;
     end;
  end;

  if sid > 0 then begin
	  MakeForm(sid);
  end;
end;

procedure TfrmKakakuKoushin.CBNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBName('Name', CBName.Text, 1);
  end;

end;

procedure TfrmKakakuKoushin.CBYomiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBName.SetFocus;
	 	MakeCBName('Yomi', CBYomi.Text, 1);
  end;
end;

{
	仕入先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmKakakuKoushin.MakeCBName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;

  sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('SID').AsString +
       ',' + FieldByName('Name').AsString +
       ',' + FieldByName('Irisuu').AsString +
       ',' + FieldByName('Kikaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with あ

  CBName.DroppedDown := True;
end;

//SIDからフォームを表示する
procedure TfrmKakakuKoushin.MakeForm(sid:integer);
var
	sSql, sSql2, sDate, sShiiresakiCode : String;
  i : Integer;
begin
	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' SID =  ' + IntToStr(sid);
  with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
//		LabelSID.Caption  := FieldbyName('SID').AsString;   
		CBYomi.Text       := FieldbyName('Yomi').AsString;
		CBCode1.Text     := FieldbyName('Code1').AsString;
		CBCode2.Text     := FieldbyName('Code2').AsString;
    tanka420.Caption := FieldbyName('Tanka420').AsString;
    tanka450.Caption := FieldbyName('Tanka450').AsString;
    end;
    Close;
  end;

end;

function NumericSort(St: TStringList; Index1, Index2: Integer): Integer;
var V1,V2:Double;
begin
//  Result := CompareStr(Copy(List[Index1], 5, 6), Copy(List[Index2], 5, 6));
  V1:=StrToFloat(St[Index1]);
  V2:=StrToFloat(St[Index2]);

  if gSort = 1 then begin
    if V1=V2 then result:=0
  else if V1<V2 then result:=1
    else result:=-1;
  end else begin
  if V1=V2 then result:=0
    else if V1>V2 then result:=1
  else result:=-1;
  end;
end;

function StringSort(St: TStringList;
                       Index1, Index2: Integer): Integer;
var
  S1, S2: string;
begin

  S1 := St[Index1] ;
  S2 := St[Index2];

  // 比較する
  if St.CaseSensitive then begin
    if gSort = 1 then begin
      Result := -AnsiCompareStr(S1, S2)
    end else begin
      Result := AnsiCompareStr(S1, S2)
    end;
  end else begin
    if gSort = 1 then begin
      Result := -AnsiCompareText(S1, S2);
    end else begin
      Result := AnsiCompareText(S1, S2);
    end;
  end;

end;

Procedure TfrmKakakuKoushin.GridSort(Grid : TStringGrid;SortCol : LongInt);
var
   St, St2 : TStringList;
   i : Integer;
   sSortString: String;
   sBase : String;

begin
     St := TStringList.Create;
     For i := Grid.FixedRows To Grid.RowCount - 1 Do
     Begin
        if Grid.Cells[SortCol,i] = '' then begin
            sBase := '0';
        end else begin
            sBase := Grid.Cells[SortCol,i];
        end;

        if SortCol = 0 then begin
           St.Add(sBase + Grid.Cells[1,i] + Grid.Cells[2,i]);
        end else if SortCol = 1 then begin
           //St.Add(sBase + Grid.Cells[2,i]);
		   St.Add(sBase);
        end else begin
           St.Add(sBase);
        end;
     End;
	 
     For i := Grid.FixedRows To Grid.RowCount - 1 Do
     Begin
       St2 := TStringList.Create;
       St2.Assign(Grid.Rows[i]);
       St.Objects[i - Grid.FixedRows] := St2;
     End;
     //St.Sort;

     case SortCol of

      {CintItemCheck,}CintItemCode1:
        //koko3
         St.CustomSort(NumericSort);
     else
         St.CustomSort(StringSort);
     end;

     //St.CustomSort(NumericSort);

     For i := Grid.FixedRows To Grid.RowCount - 1 Do
     Begin
        Grid.Rows[i].Assign(TStringList(St.Objects[i - Grid.FixedRows]));
        TStringList(St.Objects[i - Grid.FixedRows]).Free;
     End;
     St.Free;
     if GSort = 1 then begin
      gSort := 0;
     end else begin
      gSort := 1;
     end;
end;

procedure TfrmKakakuKoushin.Button2Click(Sender: TObject);
var j : Integer;

begin

  with SG1 do begin
    for j := 1 to RowCount - 1 do begin
       Cells[CintItemPrice,j] := Memo.Text;
       if(Memo.Text <> '') then begin
           SG1.Cells[CintItemCheck, j] := CStrChkValue;
       end else begin
           SG1.Cells[CintItemCheck, j] := '0';
        end;
    end;
  end;
	
end;

procedure TfrmKakakuKoushin.BitBtn3Click(Sender: TObject);
var sSql, sKeyTanka, sKeyCode, sNewTanka, sOldCode : String;
var j : Integer;
begin
 inherited;
	sKeyTanka := 'Tanka';
	sKeyCode := 'SID';
	

case MessageDlg('チェックが入った得意先の価格を一括更新します。' + #13#10 + 'よろしいですか？', mtConfirmation, [mbOK, mbCancel], 0) of
mrOk:
    begin
      with SG1 do begin
		  for j := 1 to RowCount - 1 do begin
      if(Cells[CintItemCheck, j] = CStrChkValue) then begin

			sNewTanka := Cells[CintItemPrice, j];
			sOldCode := Cells[CintItemSID, j];
			
			sSql:='Update '+CtblMItem2+' set '+sKeyTanka+'='+sNewTanka+' where '+sKeyCode+'='+sOldCode;
				  
			  With Query1 do
			  begin
				try
				  SQL.Clear;
				  SQL.Add(sSql);
				  ExecSQL;
				except
				end;           
			  end;

       end; 
		  end; // end for j
	  end; //for with
	Application.MessageBox('価格一括更新処理が完了しました。', 'Information', MB_OK);
    end;
    end;
end;


function TfrmKakakuKoushin.GetCourseID(CourseName: String) : String;
var
	sSql       : String;
  sCourseID  : String;

begin

  // コースID
	sSql := 'SELECT CourseID FROM ' + CtblMCourse;
  sSql := sSql + ' WHERE CourseName = ' + '''' + CourseName + '''';
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sCourseID :=FieldbyName('CourseID').AsString;
    Close;
  end;

  Result := sCourseID;

end;

procedure TfrmKakakuKoushin.SG1DblClick(Sender: TObject);
begin
with SG1 do
  begin

    case SG1.Col of
     0,1,2,3:
     exit;

    end;

  end ;
end;

procedure TfrmKakakuKoushin.SG1KeyPress(Sender: TObject; var Key: Char);
var
   AcceptStr:string;
begin
if ((SG1.Col = CintItemPrice)) And (SG1.Row > 0) then
  begin
    AcceptStr := '0123456789.';

    if (Pos(Key,AcceptStr)=0) and (Ord(Key) <> VK_BACK) and (Ord(Key) <> VK_RETURN) then

      Key := #00

    else

      Key := Key;

  end else begin

    Key := #00;


 end;

end;

procedure TfrmKakakuKoushin.MemoKeyPress(Sender: TObject; var Key: Char);
var
   AcceptStr:string;
begin
      AcceptStr := '0123456789.';

      if (Pos(Key,AcceptStr)=0) and (Ord(Key) <> VK_BACK) then
        Key := #00
      else if (Ord(Key) <> VK_RETURN) then
        exit
      else
       Key := Key;
end;

procedure TfrmKakakuKoushin.SG1SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
const
    MagicValue = 'd0308|ybh<_lfds$t083q()'#1#5; // 入力値としてありえない値
    PreviousEditorValue: string = MagicValue;  
begin
//
if ((SG1.Col = CintItemPrice))And (SG1.Row > 0) then
    Begin
      if Value<>PreviousEditorValue then begin    // まだ編集中
          PreviousEditorValue:= Value;
          Exit;
      end;

      case SG1.Col of
      CintItemPrice:
        if SG1.Cells[CintItemPrice, SG1.Row] <> '' then  begin
           SG1.Cells[CintItemCheck, SG1.Row] := CStrChkValue;
           SG1.Objects[CintItemCheck,   SG1.Row] := TObject(1);  //CheckBoxのcheck
        end else begin
           SG1.Cells[CintItemCheck, ARow] := '0';
        end;
      end;
      PreviousEditorValue:= MagicValue; 
    end;      
end;

end.
