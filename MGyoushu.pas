unit MGyoushu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db,
  DBTables, DBCtrls;

type
  TfrmMGyoushu = class(TfrmMaster)
    DBGrid1: TDBGrid;
    Query1: TQuery;
    DS1: TDataSource;
    BitBtn3: TBitBtn;
    Query1ID: TIntegerField;
    Query1Code: TStringField;
    Query1Name: TStringField;
    Query1FieldCode: TStringField;
    EditCode: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    EditName: TEdit;
    DBNavigator1: TDBNavigator;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmMGyoushu: TfrmMGyoushu;

implementation
uses
	inter, DM1;

{$R *.DFM}

procedure TfrmMGyoushu.FormActivate(Sender: TObject);
var
	sSql : String;
begin
  inherited;
  sSql := 'SELECT * FROM tblMList';
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' FieldCode=''GS''';
	with query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
  	Open;
  end;//of with
end;

procedure TfrmMGyoushu.FormCreate(Sender: TObject);
begin
  inherited;
	width  := 450;
  height := 500;

end;

procedure TfrmMGyoushu.BitBtn3Click(Sender: TObject);
var
	sCode, sName : String;
	InRecArry : TInRecArry;
  i : integer;
begin

	Query1.Close;

	sCode := EditCode.Text;
	sName := EditName.Text;

  //まず削除
  i := 1;
  InRecArry[i, 1]:='FieldCode';
  InRecArry[i, 2]:='GS';
  InRecArry[i, 3]:='''';
  i := i + 1;
  InRecArry[i, 1]:='Code';
  InRecArry[i, 2]:=sCode;
  InRecArry[i, 3]:='''';
  i := i + 1;
  InRecArry[i, 1]:='END';
  InRecArry[i, 2]:='END';
  InRecArry[i, 3]:='''';
	DM1.DeleteRecord(InRecArry, CtblMList);

  //次に追加
  i := 1;
  InRecArry[i, 1]:='Code';
  InRecArry[i, 2]:=sCode;
  InRecArry[i, 3]:='''';

  i := i + 1;
  InRecArry[i, 1]:='Name';
  InRecArry[i, 2]:=sName;
  InRecArry[i, 3]:='''';

  i := i + 1;
  InRecArry[i, 1]:='FieldCode';
  InRecArry[i, 2]:='GS';
  InRecArry[i, 3]:='''';

  i := i + 1;
  InRecArry[i, 1]:='END';
  InRecArry[i, 2]:='END';
  InRecArry[i, 3]:='''';

	DM1.InsertRecord(InRecArry, CtblMList);

	Query1.Open;

end;

end.
