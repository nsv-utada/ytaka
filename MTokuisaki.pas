unit MTokuisaki;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, DBTables, ShellAPI, ComCtrls;

type
  TfrmMTokuisaki = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    CBTokuisakiCode1: TComboBox;
    CBTokuisakiCode2: TComboBox;
    CBTokuisakiNameYomi: TComboBox;
    CBTokuisakiName: TComboBox;
    CBCName: TComboBox;
    CBFName: TComboBox;
    CBShiharaiKubun: TComboBox;
    CBSeikyuuSimebi: TComboBox;
    CBShiharaibiM: TComboBox;
    CBShiharaibiD: TComboBox;
    Label13: TLabel;
    CBTel: TComboBox;
    CBMemo: TMemo;
    Label14: TLabel;
    Memo2: TMemo;
    BitBtn3: TBitBtn;
    CBZanOnOff: TComboBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    CBZip: TComboBox;
    CBAdd1: TComboBox;
    CBAdd3: TComboBox;
    CBAdd2: TComboBox;
    RMemo: TMemo;
    Label20: TLabel;
    BitBtn4: TBitBtn;
    edTokuisakiNameUp: TEdit;
    Label21: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CBShiharaiKubunExit(Sender: TObject);
    procedure CBSeikyuuSimebiExit(Sender: TObject);
    procedure CBZanOnOffExit(Sender: TObject);
    procedure CBShiharaibiMExit(Sender: TObject);
    procedure CBShiharaibiDExit(Sender: TObject);
    procedure CBTokuisakiNameYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiCode1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTelKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
		procedure MakeForm(sTokuisakiCode1:String);
		procedure UpdateTokuisaki(sTokuisakiCode1:String);
		procedure InsertTokuisaki;
  public
    { Public 宣言 }
  end;

var
  frmMTokuisaki: TfrmMTokuisaki;

	function GetTokuisakiName(sTokuisakiCode1:String):String;
	function GetTokuisakiCode2(sTokuisakiCode1:String):String;
	function GetShimebi(sTokuisakiCode1:String):String;
	function GetZanOnOff(sTokuisakiCode1 : String) : Integer;
	function CheckTokuisakiCode(sTokuisakiCode1:String):String;

	procedure EditMaster(iID:Integer;sGatsubun:String);

implementation
uses
	DMMaster, Inter, HKLib;

{$R *.DFM}

procedure TfrmMTokuisaki.FormCreate(Sender: TObject);
begin
  inherited;
  Width := 600;
  Height := 540;
end;

//得意先名で検索
procedure TfrmMTokuisaki.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
  	MakeCBTokuisakiName('TokuisakiName', CBTokuisakiName.Text, 1);
  end;

end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmMTokuisaki.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;
  
  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' + FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;

procedure TfrmMTokuisaki.CBTokuisakiNameExit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	//得意先コード１を取得して得意先を検索する
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));
  if sTokuisakiCode1 <> '' then
	  MakeForm(sTokuisakiCode1);
  //frmMTokuisaki.Update;
end;

//得意先コードから得意先マスターフォームを表示する
//1999/03/07
procedure TfrmMTokuisaki.MakeForm(sTokuisakiCode1:String);
var
	sSql : String;
begin
	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
			CBTokuisakiCode1.Text := sTokuisakiCode1;
			CBTokuisakiCode2.Text := FieldbyName('TokuisakiCode2').AsString;
			CBTokuisakiNameYomi.Text := FieldbyName('TokuisakiNameYomi').AsString;
			CBTokuisakiName.Text := FieldbyName('TokuisakiName').AsString;
			CBCName.Text := FieldbyName('CName').AsString;
			CBFName.Text := FieldbyName('FName').AsString;
			CBShiharaiKubun.Text := FieldbyName('ShiharaiKubun').AsString;
			CBSeikyuuSimebi.Text := FieldbyName('SeikyuuSimebi').AsString;
			CBShiharaibiM.Text := FieldbyName('ShiharaibiM').AsString;
			CBShiharaibiD.Text := FieldbyName('ShiharaibiD').AsString;
			CBTel.Text := FieldbyName('Tel').AsString;
			CBZanOnOff.Text := FieldbyName('ZanOnOff').AsString;
			CBMemo.Text := FieldbyName('Memo').AsString;
			RMemo.Text := FieldbyName('EstimateMemo').AsString;

      //1999/05/17追加
      CBZip.Text := FieldbyName('Zip').AsString;
      CBAdd1.Text := FieldbyName('Add1').AsString;
      CBAdd2.Text := FieldbyName('Add2').AsString;
      CBAdd3.Text := FieldbyName('Add3').AsString;

      //2002.08.10
      CBAdd3.Text := FieldbyName('TokuisakiNameUp').AsString;
    end;
    Close;
  end;
end;

//得意先コードが登録されているかチェックする
//1999/08/10
function CheckTokuisakiCode(sTokuisakiCode1:String):String;
var
	sSql : String;
begin
	sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryTokuisakiName do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('TokuisakiCode1').AsString;
    end else begin
    	Result := '';
    end;
    Close;
  end;//of with
end;


//登録ボタンがクリックされた
//1999/03/07
procedure TfrmMTokuisaki.BitBtn3Click(Sender: TObject);
var
	sTokuisakiName, sTokuisakiCode1 : String;
begin
	//データチェック
  if CBTokuisakiCode1.Text = '' then begin
  	ShowMessage('得意先コードは省略できません');
    CBTokuisakiCode1.SetFocus;
    Exit;
  end;
  if CBTokuisakiName.Text = '' then begin
  	ShowMessage('得意先名は省略できません');
    CBTokuisakiName.SetFocus;
    Exit;
  end;

	//２重登録の確認
  sTokuisakiCode1 := CBTokuisakiCode1.Text;
  sTokuisakiCode1 := CheckTokuisakiCode(sTokuisakiCode1);
  //sTokuisakiName := GetTokuisakiName(sTokuisakiCode1);
  try
	  if sTokuisakiCode1 = '' then begin
			//確認メッセージ
			if MessageDlg('新規データです。登録しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		InsertTokuisaki;
	  end else begin
			//確認メッセージ
			if MessageDlg('既存データです。修正しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		UpdateTokuisaki(sTokuisakiCode1);
	  end;
  	ShowMessage('登録に成功しました');
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
		  ShowMessage('登録に失敗しました');
    end;
  end;

end;

//得意先マスターの更新
//1999/03/08
procedure TfrmMTokuisaki.UpdateTokuisaki(sTokuisakiCode1:String);
var
	sSql : String;
begin
	sSql := 'UPDATE ' + CtblMTokuisaki;
  sSql := sSql + ' SET ';
  sSql := sSql + 'TokuisakiCode2    = '   + CBTokuisakiCode2.Text    + ',';
  sSql := sSql + 'TokuisakiName     = ''' + CBTokuisakiName.Text     + ''',';
  sSql := sSql + 'TokuisakiNameYomi = ''' + CBTokuisakiNameYomi.Text + ''',';
  sSql := sSql + 'CName             = ''' + CBCName.Text             + ''',';
  sSql := sSql + 'FName             = ''' + CBFName.Text             + ''',';
  sSql := sSql + 'ShiharaiKubun     = '   + CBShiharaiKubun.Text     + ',';
  sSql := sSql + 'SeikyuuSimebi     = '   + CBSeikyuuSimebi.Text     + ',';
  sSql := sSql + 'ShiharaibiM       = '   + CBShiharaibiM.Text       + ',';
  sSql := sSql + 'ShiharaibiD       = '   + CBShiharaibiD.Text       + ',';
  sSql := sSql + 'Tel               = ''' + CBTel.Text               + ''',';
  sSql := sSql + 'Memo              = ''' + CBMemo.Text              + ''',';

  sSql := sSql + 'EstimateMemo      = ''' + RMemo.Text              + ''',';


  //1999/05/17 追加
  sSql := sSql + 'Zip               = ''' + CBZip.Text              + ''',';
  sSql := sSql + 'Add1              = ''' + CBAdd1.Text              + ''',';
  sSql := sSql + 'Add2              = ''' + CBAdd2.Text              + ''',';
  sSql := sSql + 'Add3              = ''' + CBAdd3.Text              + ''',';

  if CBZanOnOff.Text = '' then CBZanOnOff.Text := '1';//標準で載せる
  sSql := sSql + 'ZanOnOff          = '   + Copy(CBZanOnOff.Text, 1, 1);

	sSql := sSql + ' WHERE TokuisakiCode1 = ' + sTokuisakiCode1;
  with frmDMMaster.QueryUpdate do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with
end;

//得意先マスターの登録
//1999/03/08
procedure TfrmMTokuisaki.InsertTokuisaki;
var
	sSql : String;
begin
{
  sSql := sSql + 'TokuisakiCode2    = ''' + CBTokuisakiCode2.Text    + ''',';
  sSql := sSql + 'TokuisakiName     = ''' + CBTokuisakiName.Text     + ''',';
  sSql := sSql + 'TokuisakiNameYomi = ''' + CBTokuisakiNameYomi.Text + ''',';
  sSql := sSql + 'CName             = ''' + CBCName.Text             + ''',';
  sSql := sSql + 'FName             = ''' + CBFName.Text             + ''',';
  sSql := sSql + 'ShiharaiKubun     = '   + CBShiharaiKubun.Text     + ',';
  sSql := sSql + 'SeikyuuSimebi     = '   + CBSeikyuuSimebi.Text     + ',';
  sSql := sSql + 'ShiharaibiM       = '   + CBShiharaibiM.Text       + ',';
  sSql := sSql + 'ShiharaibiD       = '   + CBShiharaibiD.Text       + ',';
  sSql := sSql + 'Tel               = ''' + CBTel.Text               + ''',';
  sSql := sSql + 'Memo              = ''' + CBMemo.Text              + ''',';
  sSql := sSql + 'ZanOnOff          = '   + CBZanOnOff.Text;
}

   	sSql := 'INSERT INTO ' + CtblMTokuisaki;
  	sSql := sSql + '(';
    sSql := sSql + 'TokuisakiCode1,    ';
		sSql := sSql + 'TokuisakiCode2,    ';
		sSql := sSql + 'TokuisakiName,     ';
	  sSql := sSql + 'TokuisakiNameYomi, ';
	  sSql := sSql + 'CName,             ';
	  sSql := sSql + 'FName,             ';
	  sSql := sSql + 'ShiharaiKubun,     ';
	  sSql := sSql + 'SeikyuuSimebi,     ';
	  sSql := sSql + 'ShiharaibiM,       ';
	  sSql := sSql + 'ShiharaibiD,       ';
	  sSql := sSql + 'Tel,               ';
	  sSql := sSql + 'Memo,              ';
    //1999/06/21 追加
	  sSql := sSql + 'EstimateMemo,              ';

    //1999/05/17 追加
	  sSql := sSql + 'Zip,              ';
	  sSql := sSql + 'Add1,              ';
	  sSql := sSql + 'Add2,              ';
	  sSql := sSql + 'Add3,              ';

    //2002.08.10
	  sSql := sSql + 'TokuisakiNameUp,              ';

	  sSql := sSql + 'ZanOnOff           ';
		sSql := sSql + ') VALUES (';
		sSql := sSql + CBTokuisakiCode1.Text    + ',';
		sSql := sSql + CBTokuisakiCode2.Text    + ',';
		sSql := sSql + '''' + CBTokuisakiName.Text     + ''',';
		sSql := sSql + '''' + CBTokuisakiNameYomi.Text + ''',';
		sSql := sSql + '''' + CBCName.Text             + ''',';
		sSql := sSql + '''' + CBFName.Text             + ''',';
		sSql := sSql        + CBShiharaiKubun.Text     + ',';
		sSql := sSql        + CBSeikyuuSimebi.Text     + ',';
		sSql := sSql        + CBShiharaibiM.Text       + ',';
		sSql := sSql        + CBShiharaibiD.Text       + ',';
		sSql := sSql + '''' + CBTel.Text               + ''',';
		sSql := sSql + '''' + CBMemo.Text              + ''',';
		sSql := sSql + '''' + RMemo.Text              + ''',';


    //1999/05/17 追加
		sSql := sSql + '''' + CBZip.Text              + ''',';
		sSql := sSql + '''' + CBAdd1.Text              + ''',';
		sSql := sSql + '''' + CBAdd2.Text              + ''',';
		sSql := sSql + '''' + CBAdd3.Text              + ''',';

    //2002.08.10
		sSql := sSql + '''' + edTokuisakiNameUp.Text   + ''',';

		sSql := sSql        + Copy(CBZanOnOff.Text, 1, 1);
    sSql := sSql + ')';
   	with frmDMMaster.QueryInsert do begin
		 	Close;
  	  Sql.Clear;
   		Sql.Add(sSql);
   		ExecSql;
      Close;
    end;//of with
end;

//得意先名を取得する
//1999/03/07
//2002.08.30 変更
function GetTokuisakiName(sTokuisakiCode1:String):String;
var
	sSql : String;
  sName : String;
begin
	if sTokuisakiCode1 = '' then begin
    	Result := '';
			Exit;
  end;
	sSql := 'SELECT TokuisakiName,TokuisakiNameUp FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryTokuisakiName do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	sName := Trim(FieldByName('TokuisakiNameUp').AsString);
    	Result := sName + '　' + FieldByName('TokuisakiName').AsString;
    end else begin
    	Result := '';
    end;
    Close;
  end;//of with
end;

//得意先コード２を取得する
//1999/03/07
function GetTokuisakiCode2(sTokuisakiCode1:String):String;
var
	sSql : String;
begin
	sSql := 'SELECT TokuisakiCode2 FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryTokuisakiName do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('TokuisakiCode2').AsString;
    end else begin
    	Result := '';
    end;
    Close;
  end;//of with
end;

//締め日を取得する
//1999/04/24
function GetShimebi(sTokuisakiCode1:String):String;
var
	sSql : String;
begin
	sSql := 'SELECT SeikyuuSimebi FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryTokuisakiName do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('SeikyuuSimebi').AsString;
    end else begin
    	Result := '';
    end;
    Close;
  end;//of with
end;

//請求書に残をのせるのせないを取得する
//1999/06/15
function GetZanOnOff(sTokuisakiCode1 : String) : Integer;
var
	sSql : String;
begin
	sSql := 'SELECT ZanOnOff FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryTokuisakiName do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('ZanOnOff').AsInteger;
    end else begin
    	ShowMessage(sTokuisakiCode1 + 'の請求算をのせるのせないのデータが不正です');
    	Result := 1;
    end;
    Close;
  end;//of with
end;

procedure TfrmMTokuisaki.CBShiharaiKubunExit(Sender: TObject);
begin
	CBShiharaiKubun.Text := Copy(CBShiharaiKubun.Text, 1, 2);
end;

procedure TfrmMTokuisaki.CBSeikyuuSimebiExit(Sender: TObject);
begin
	CBSeikyuuSimebi.Text := Trim(Copy(CBSeikyuuSimebi.Text, 1, 2));

end;

procedure TfrmMTokuisaki.CBZanOnOffExit(Sender: TObject);
begin
	CBZanOnOff.Text := Trim(Copy(CBZanOnOff.Text, 1, 1));

end;

procedure TfrmMTokuisaki.CBShiharaibiMExit(Sender: TObject);
begin
	CBShiharaibiM.Text := Trim(Copy(CBShiharaibiM.Text, 1, 1));

end;

procedure TfrmMTokuisaki.CBShiharaibiDExit(Sender: TObject);
begin
	CBShiharaibiD.Text := Trim(Copy(CBShiharaibiD.Text, 1, 2));
end;

procedure TfrmMTokuisaki.CBTokuisakiNameYomiKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiNameYomi.Text, 1);
  end;
end;

procedure TfrmMTokuisaki.CBTokuisakiCode1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('TokuisakiCode1', CBTokuisakiCode1.Text, 2);
  end;

end;

procedure TfrmMTokuisaki.CBTelKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('Tel', CBTel.Text, 0);
  end;

end;

procedure TfrmMTokuisaki.BitBtn2Click(Sender: TObject);
var
	sSql, sLine, strMemo : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('得意先一覧を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMTokuisaki;

  //テスト用
  //sSql := sSql + ' WHERE TokuisakiCode1 < 10 ';

  sSql := sSql + ' ORDER BY TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Tokuisaki);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	sLine := FieldByName('TokuisakiCode1').AsString;
    	sLine := sLine + ',' + FieldByName('TokuisakiCode2').AsString;

      //1999/08/08追加

    	sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;
    	sLine := sLine + ',' + FieldByName('TokuisakiNameYomi').AsString;
    	sLine := sLine + ',' + FieldByName('Tel').AsString;
    	sLine := sLine + ',' + FieldByName('SeikyuuSimebi').AsString;

      //1999/09/06 改行の入ったメモをExcelで利用するため
    	//sLine := sLine + ',' + FieldByName('Memo').AsString;
    	strMemo := FieldByName('Memo').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;

    	//sLine := sLine + ',' + FieldByName('EstimateMemo').AsString;
    	strMemo := FieldByName('EstimateMemo').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;


    	sLine := sLine + ',' + FieldByName('CName').AsString;
    	sLine := sLine + ',' + FieldByName('FName').AsString;


    	sLine := sLine + ',' + FieldByName('ShiharaiKubun').AsString;
    	sLine := sLine + ',' + FieldByName('ShiharaibiM').AsString;
    	sLine := sLine + ',' + FieldByName('ShiharaibiD').AsString;
    	sLine := sLine + ',' + FieldByName('Tel').AsString;
    	sLine := sLine + ',' + FieldByName('ZanOnOff').AsString;
      {
    	sLine := sLine + ',' + FieldByName('Zip').AsString;

      sLine := sLine + ',' + FieldByName('Add1').AsString;
    	sLine := sLine + ',' + FieldByName('Add2').AsString;
    	sLine := sLine + ',' + FieldByName('Add3').AsString;
      }

		  HMakeFile(CFileName_Tokuisaki, sLine);
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '得意先一覧.xls', '', SW_SHOW);
end;

//月分自動入力ボタンがクリックされた
//1999/09/10
procedure TfrmMTokuisaki.BitBtn4Click(Sender: TObject);
var
	sSql, sGatsubun, sSeikyuuShimebi, sYear,sMonth,sDay : String;
  i : Integer;
begin
	sSql := 'SELECT  ID, DDate, UriageKingaku  FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 = ' + CBTokuisakiCode1.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku > 0 ';
  with frmDMMaster.QueryTTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 1;
    while not EOF do begin
    	sGatsubun := FieldByName('DDate').AsString;

			//1999/09/10
			// TDenpyouのGatsubun に何月分の売上かを入力する
		  //伝票日付が締め日を越えていたら月を１進める
  		sSeikyuuShimebi := MTokuisaki.GetShimebi(CBTokuisakiCode1.Text);
		 	sYear := Copy(sGatsubun, 1, 2);
 			sMonth := Copy(sGatsubun, 4, 2);
	 		sDay := Copy(sGatsubun, 7, 2);
	  	if sSeikyuuShimebi = '0' then begin //月末ならそのまま
  	  	sGatsubun := sYear + '/' + sMonth + '/01';
	  	end else begin
  	  	if StrToInt(sDay) > StrToInt(sSeikyuuShimebi) then begin
	    		if StrToInt(sMonth) = 12 then begin
  	    	  sMonth := '01';
	  	    end else begin
  	  	  	sMonth := IntToStr(StrToInt(sMonth) + 1);
	    	  end;
		    end;
  		  sGatsubun := sYear + '/' + sMonth + '/01';
		  end;//of if

      EditMaster(FieldByName('ID').AsInteger, sGatsubun);

      Next;
      i := i + 1;
    end; //of while
    Close;
  end;
  ShowMessage('月分の再登録が完了しました' + IntToStr(i) + '件です');
end;

procedure EditMaster(iID:Integer;sGatsubun:String);
var
	sSql: String;
begin
	sSql := 'UPDATE ' + CtblTDenpyou ;
  sSql := sSql + ' Set Gatsubun = ''' + sGatsubun + '''';
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ID = ' + IntToStr(iID);
  with frmDMMaster.QueryTokuisakiEdit do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;
end;

end.
