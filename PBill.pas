unit PBill;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, DBTables, ShellAPI, Db;

type
  TfrmPBill = class(TfrmMaster)
    DTP1: TDateTimePicker;
    Shape1: TShape;
    Shape2: TShape;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EditTokuisakiCode1: TEdit;
    Label6: TLabel;
    LabelTokuisakiName: TLabel;
    Label8: TLabel;
    CBYyyy: TComboBox;
    BitBtn3: TBitBtn;
    Label9: TLabel;
    CBMm: TComboBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    CBDd: TComboBox;
    Label13: TLabel;
    Panel4: TPanel;
    Label7: TLabel;
    DTPFrom: TDateTimePicker;
    DTPTo: TDateTimePicker;
    CheckBox1: TCheckBox;
    Label15: TLabel;
    Label14: TLabel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    qrySeikyuu: TQuery;
    EditTantoushaCode: TEdit;
    Label24: TLabel;
    EditShuukeiCode: TEdit;
    Label16: TLabel;
    Label17: TLabel;
    EditTokuisakiCode2: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure EditTokuisakiCode1Exit(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private 宣言 }
		//Function GetBillTarm(sTokuisakiCode1:String; Var sDateFrom, sDateTo : String) : Boolean;
		procedure MakeTokuisakiBill(sTokuisakiCode1:String ; iZanOnOff:Integer);
		procedure MakeTokuisakiBill2(sTokuisakiCode1:String ; iZanOnOff:Integer);
  public
    { Public 宣言 }
  end;

Function GetShimebi(sTokuisakiCode1:String) : Integer;
Function GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;
//procedure InsertCTax2(MyQuery:TQuery; sYyyyMmDd : String);
//procedure InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer);
procedure InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer; SB1:TStatusBar);
procedure InsertCTax2(sTokuisakiCode1, sYyyyMmDd : String);
function MakeSqlUriageSum(sTokuisakiCode1, sDateFrom, sDateTo : String) : String;
function GetTokuisakiTax(sTokuisakiCode1, sGatsubun : String) : String;   // 2014.04.01 消費税対応


var
  frmPBill: TfrmPBill;

implementation

uses DMMaster, Inter, HKLib, MTokuisaki, DenpyouPrintSeikyuu;

{$R *.DFM}

procedure TfrmPBill.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
  inherited;
  width := 490;
  Height := 480;
  DTP1.Date := Date;
  DTPFrom.Date := Date;
  DTPTo.Date := Date;

   // 年・月を当日の年・月に設定
   DecodeDate(Date(), wYyyy, wMm, wDd);
    sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyy.Text := sYear;
   CBMm.Text   := sMonth;

end;


//印刷開始ボタンがクリックされた
//1999/05/04
//
//1999/05/14
//締め日より１ヶ月前までの売上に対して消費税をけいさんして
//１レコードとしてテーブルに挿入する。
//もし既にある場合でも再計算するようにする。
procedure TfrmPBill.BitBtn3Click(Sender: TObject);
var
	sLine, sSql, sTokuisakiCode1, sDateFrom, sDateTo, sTokuisakiCode1Tmp: string;
 	F : TextFile;
  iSum, iSum2, iZanOnOff : integer;
  wYyyy, wMm, wDd : Word;
  sDate,sDd : String;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //
  //メッセージを出し、過去のバージョンで作業してもらうように促す。
  //
  if CBDd.Text = '0' then begin
    sDd := '28';
  end else begin
    sDd := CBDd.Text;
  end;
  sDate := CbYyyy.Text + '/' + CBMm.Text + '/' + sDd;
  if StrToDate(sDate) < StrToDate('2004/03/28') then begin
    Showmessage('2004/3/31 以前の請求書は、Ver48にて作業してください');
    Exit;
  end;


	//スタートトランザクション

	//得意先ごとに請求書の計算をする
  if Length(EditTokuisakiCode1.Text) > 0 then
	  if Copy(EditTokuisakiCode1.Text, Length(EditTokuisakiCode1.Text) , 1) <> ',' then
  		EditTokuisakiCode1.Text := EditTokuisakiCode1.Text + ',';

  sTokuisakiCode1 := EditTokuisakiCode1.Text;

  //請求期間の消費税を入力する
  //すでに消費税の伝票がある場合には上書きする
	SB1.SimpleText := '消費税の計算中 ';
  if (Length(sTokuisakiCode1) > 0) then begin
  	//2000/01/22 得意先コードの複数指定に対応
    while sTokuisakiCode1 <> '' do begin
	    sTokuisakiCode1Tmp := Copy(sTokuisakiCode1, 1, Pos(',', sTokuisakiCode1)-1);

		  CBDd.Text := IntToStr(GetShimebi(sTokuisakiCode1Tmp));
      //2002.09.29
      if CBDd.Text = '99' then begin //閉め日が入力されていない
      	CBDd.Text := '';
      end;
  	  InsertCTax(CBYyyy.Text, CBMm.Text, CBDd.Text, StrToInt(sTokuisakiCode1Tmp), SB1);
      sTokuisakiCode1 := Copy(sTokuisakiCode1, (Pos(',', sTokuisakiCode1)+1), Length(sTokuisakiCode1));
			SB1.SimpleText := '消費税の計算中 ' + sTokuisakiCode1;
      SB1.Update;
    end;//of while
	end else begin
    InsertCTax(CBYyyy.Text, CBMm.Text, CBDd.Text, 0, SB1);
  end;
	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Seikyuu);
  Rewrite(F);
	CloseFile(F);

	SB1.SimpleText := '請求金額計算中 ';
  Update;

  //2000/01/23
  sTokuisakiCode1 := EditTokuisakiCode1.Text;

  if (Length(sTokuisakiCode1) > 0) then begin
  	//2000/01/22 得意先コードの複数指定に対応
    while sTokuisakiCode1 <> '' do begin
	    sTokuisakiCode1Tmp := Copy(sTokuisakiCode1, 1, Pos(',', sTokuisakiCode1)-1);
		  CBDd.Text := IntToStr(GetShimebi(sTokuisakiCode1Tmp));
      //2002.09.29
      if CBDd.Text = '99' then begin //閉め日が入力されていない
      	CBDd.Text := '';
        ShowMessage('閉め日のデータが不正なので処理を中止します');
        exit;
      end;
    	iZanOnOff := MTokuisaki.GetZanOnOff(sTokuisakiCode1Tmp);
			MakeTokuisakiBill2(sTokuisakiCode1Tmp, iZanOnOff);
      sTokuisakiCode1 := Copy(sTokuisakiCode1, (Pos(',', sTokuisakiCode1)+1), Length(sTokuisakiCode1));
    end;//of while
  end else begin
  	sSql := 'SELECT TokuisakiCode1, ZanOnOff FROM ' + CtblMTokuisaki;
    sSql := sSql + ' WHERE SeikyuuSimebi = ' + Trim(CBDd.Text);

    //2000/01/22 請求書フラグを見る　１は出さない　０又はヌルは出す。
    sSql := sSql + ' AND ';
    sSql := sSql + ' SeikyuushoFlag <> 1 ';

    //テスト用　2000/09/27
    //sSql := sSql + ' AND ';
    //sSql := sSql + ' TokuisakiCode1 BETWEEN 2630 AND 2691';
    //ここまで

    sSql := sSql + ' AND ';
    sSql := sSql + ' TokuisakiCode1 < 50000 ';

    sSql := sSql + ' ORDER BY TokuisakiCode1';
    with frmDMMaster.QueryMTokuisaki do begin
	  	Close;
  	  Sql.Clear;
    	Sql.Add(sSql);
	    open;
      while not EOF do begin
				SB1.SimpleText := '請求金額計算中 -> '+FieldByName('TokuisakiCode1').AsString;
        iZanOnOff := FieldByName('ZanOnOff').AsInteger;
        sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
				MakeTokuisakiBill2(sTokuisakiCode1, iZanOnOff);
      	Next;
      end;//of while
      Close;
    end;//of with
  end;//of if

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '請求書new.xls', '', SW_SHOW);
end;


//得意先ごとに請求書の計算をする
//iZanOnOff -> 0 のせない 1 のせる
procedure TfrmPBill.MakeTokuisakiBill(sTokuisakiCode1:String ; iZanOnOff:Integer);
var
	sYuusousaki, sWhere, sTokuisakiCode2, sLine, sSql, sDateFrom, sDateTo, sSum2 : string;
 	F : TextFile;
  iSum, iSum2 : integer;
  wYyyy, wMm, wDd : Word;
  wYyyy2, wMm2, wDd2 : Word;
  sTokusakiTax,sGatsubun : String; //2014.04.01 消費税対応
begin
  sSql := 'SELECT UriageSum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
 	sSql := sSql + ' AND ';

  //請求期間の取得
  //請求締め日の１ヶ月前を取得
  if CBDd.Text = '0' then begin //締め日が月末
  	wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end else begin
  	wDd := StrToint(CBDd.Text);
  end;
  sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);
	DecodeDate(StrToDate(sDateTo), wYyyy, wMm, wDd);

  //
  if CBDd.Text = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  sDateFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));


	sSql := sSql + 'DDate Between ';

  //1999/08/08追加
  //請求期間を強制的に変更する
  {回収のみ
  if CheckBox1.Checked = True then begin
    sDateFrom := DateToStr(DTPFrom.Date);
    sDateTo   := DateToStr(DTPTo.Date);
  end;
  }
	sSql := sSql + '''' + sDateFrom + '''' + ' AND ';
	sSql := sSql + '''' + sDateTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

	//1999/08/09
	sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない


  //当月請求金額の算出
  with frmDMMaster.QueryUriage do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql); //税込みのsql文になっているしかも返品の考慮なし
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //前月請求残高の取得
  //前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
	if iZanOnOff = 1 then begin

	  //1999/08/08追加
  	//請求期間を強制的に変更する
  	if CheckBox1.Checked = True then begin
    	//sDateFrom := DateToStr(DTPFrom.Date); //1999/09/25
    	sDateTo   := DateToStr(DTPTo.Date);
  	end;
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);
  end else begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);
    if iSum2 < 0 then begin
    	;//前月末残高がマイナスのときは出力する
    end else begin
	  	iSum2 := 0;
  	  sSum2 := '　';
    end;
  end;

  if (iSum = 0) AND (iSum2 = 0) then begin
  	;
  end else begin
	  //得意先名の取得
    sTokuisakiCode2 := MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1);
  	sLine := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);
	  sLine := sLine + ',"' + sTokuisakiCode1+ '−'+ sTokuisakiCode2 + '",' + IntToStr(iSum) + ',' + sSum2;

    //1999/07/15 請求締め日から発行日に変更
	  //1999/07/25 発行日から請求締め日に戻す
    //sLine := sLine + ',' + DateTostr(DTP1.Date);
	  sLine := sLine + ',' + sDateTo;

    sLine := sLine + ',' + CBMm.Text; //月分

    //Ver2.0 で追加
    sLine := sLine + ',' + sTokuisakiCode2; //得意先コード２
    sWhere := 'TokuisakiCode1 = ' + sTokuisakiCode1;
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'Ka', sWhere);//課
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TantoushaCode', sWhere);//担当者コード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'ChainCode', sWhere);//チェーンコード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiNameYomi', sWhere);//よみ
    sYuusousaki :=DMMaster.GetFieldData(CtblMTokuisaki, 'SeikyuushoHassouSaki', sWhere);//請求書発行先 郵送、持参
    if sYuusousaki = '0' then sYuusousaki := '持参' else sYuusousaki := '郵送';
    sLine := sLine + ',' + sYuusousaki;


    //2014.04.01 消費税対応 beggn
    sGatsubun :=  CBYyyy.Text + '/' +  CBMm.Text + '01';
    sTokusakiTax := GetTokuisakiTax(sTokuisakiCode1,sGatsubun);
    sLine := sLine + ',' + sTokusakiTax;
    // end


    HMakeFile(CFileName_Seikyuu, sLine);
  end;//of if

end;


//得意先ごとに請求書の計算をする
//iZanOnOff -> 0 のせない 1 のせる
procedure TfrmPBill.MakeTokuisakiBill2(sTokuisakiCode1:String ; iZanOnOff:Integer);
var
	sTokuisakiCode2, sLine, sSql, sDateFrom, sDateTo, sSum2 : string;
 	F : TextFile;
  iSum, iSum2 : integer;
  wYyyy, wMm, wDd : Word;
  wYyyy2, wMm2, wDd2 : Word;
  sWhere, sYuusousaki, sShuukei : String;
  sTokusakiTax,sGatsubun : String; //2014.04.01 消費税対応

begin
  sSql := 'SELECT UriageSum = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

 	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
 	sSql := sSql + ' AND ';

  //請求期間の取得
  //請求締め日の１ヶ月前を取得
  if CBDd.Text = '0' then begin //締め日が月末
  	wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end else begin
  	wDd := StrToint(CBDd.Text);
  end;
  sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);
	DecodeDate(StrToDate(sDateTo), wYyyy, wMm, wDd);

  if CBDd.Text = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  sDateFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	sSql := sSql + 'DDate Between ';

  //1999/08/08追加
  //請求期間を強制的に変更する
  {回収のみ
  if CheckBox1.Checked = True then begin
    sDateFrom := DateToStr(DTPFrom.Date);
    sDateTo   := DateToStr(DTPTo.Date);
  end;
  }
	sSql := sSql + '''' + sDateFrom + '''' + ' AND ';
	sSql := sSql + '''' + sDateTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0 ';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1 ,2) IN (''返品'', ''値引'')))' ;
	sSql := sSql + ' AND ';
	sSql := sSql + ' Memo <> ''' + CsCTAX + '''';//消費税ではない

	//1999/08/09
	sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない

  //当月請求金額の算出
  with frmDMMaster.QueryUriage do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql); //税込みのsql文になっているしかも返品の考慮なし
    open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //請求期間を強制的に変更する
  //2000/03/23
  if CheckBox1.Checked = True then begin
    	sDateTo   := DateToStr(DTPTo.Date);
  end;

  //前月請求残高の取得
  //前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
	if iZanOnOff = 1 then begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);
  end else begin
	  iSum2 := GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo);
	  sSum2 := IntToStr(iSum2);
    if iSum2 < 0 then begin
    	;//前月末残高がマイナスのときは出力する
    end else begin
	  	iSum2 := 0;
  	  sSum2 := '　';
    end;
  end;

  if (iSum = 0) AND (iSum2 = 0) then begin
  	;
  end else begin
	  //得意先名の取得
    sTokuisakiCode2 := MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1);
  	sLine := MTokuisaki.GetTokuisakiName(sTokuisakiCode1);
    //Ver2.0
	  //sLine := sLine + ',"' + sTokuisakiCode1+ '−'+ sTokuisakiCode2 + '",' + IntToStr(iSum) + ',' + sSum2;

    //2001/07/18
    //月の請求金額がマイナスの場合？
    //iSumは税込みになっている
    //sSum2(前月までの請求残)はどうなってるの？
    //
    if (iSum<0) then begin
    	sSum2 := '　';
      //iSum := StrToInt(Real2Str(iSum/1.05,0,0));
      //iSum := StrToInt(Real2Str(iSum*1.05,0,0));
	  	sLine := sLine + ',' + sTokuisakiCode1+ ',' + IntToStr(iSum) + ',' + sSum2;
    end else begin
	  	sLine := sLine + ',' + sTokuisakiCode1+ ',' + IntToStr(iSum) + ',' + sSum2;
    end;//of if

    //1999/07/15 請求締め日から発行日に変更
	  //1999/07/25 発行日から請求締め日に戻す
    //sLine := sLine + ',' + DateTostr(DTP1.Date);
	  sLine := sLine + ',' + sDateTo;

    sLine := sLine + ',' + CBMm.Text; //月分

    //Ver2.0 で追加
    sWhere := 'TokuisakiCode1 = ' + sTokuisakiCode1;
    sLine := sLine + ',' + sTokuisakiCode2; //得意先コード２
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'Ka', sWhere);//課
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TantoushaCode', sWhere);//担当者コード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'ChainCode', sWhere);//チェーンコード
    sLine := sLine + ',' + DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiNameYomi', sWhere);//よみ

    sYuusousaki :=DMMaster.GetFieldData(CtblMTokuisaki, 'SeikyuushoHassouSaki', sWhere);//請求書発行先 郵送、持参
    if sYuusousaki = '0' then begin
    	sYuusousaki := '持参';
    end else if sYuusousaki = '1' then begin
    	sYuusousaki := '郵送(会社)';
    end else if sYuusousaki = '2' then begin
    	sYuusousaki := '郵送(店)';
    end else if sYuusousaki = '3' then begin
    	sYuusousaki := '郵送(自宅)';
    end else begin
    	sYuusousaki := 'データ不正';
    end;

    sLine := sLine + ',' + sYuusousaki;

    //2002.10.31 集計コードを追加
    sShuukei := DMMaster.GetFieldData(CtblMTokuisaki, 'ShuukeiCode', sWhere);
    sLine := sLine + ',' + sShuukei;

    //2014.04.01 消費税対応 beggn
    sGatsubun :=  CBYyyy.Text + '/' +  CBMm.Text + '/01';
    sTokusakiTax := GetTokuisakiTax(sTokuisakiCode1,sGatsubun);
    sLine := sLine + ',' + sTokusakiTax;
    // end


    HMakeFile(CFileName_Seikyuu, sLine);
  end;//of if

end;


//請求期間の消費税を入力する
//請求締め日を同じくする得意先のsYyyy, sMmの消費税の伝票を
//すべて再計算する
//すでに消費税の伝票がある場合には上書きする
//引数 sYyyy->
//     sMm  ->
//     sDd  -> 請求締め日
//     iTokuisakiCode1 -> 0ならば締め日の同じものすべて
//
procedure InsertCTax(sYyyy, sMm, sDd:String ; iTokuisakiCode1 : Integer; SB1:TStatusBar);
var
	sSql, sSqlDel, sSqlIn, sDd2 : String;
begin
	//20020929
  if sDd='' then begin //得意先の閉め日がない又は得意先がない場合
  	Exit;
  end;
	//請求締め日で得意先コードを抽出する
  if iTokuisakiCode1 = 0 then begin
		sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
  	sSql := sSql + ' WHERE ';
	  sSql := sSql + 'SeikyuuSimebi = ' + sDd;
  	sSql := sSql + ' AND ';
	  sSql := sSql + 'TokuisakiCode1 < 50000';
  end else begin
  	sSql := IntToStr(iTokuisakiCode1);
  end;

  //sDdをその月の月末日に変換する
  if StrToInt(sDd) = CSeikyuuSimebi_End then begin
  	sDd2 := IntToStr(HKLib.GetGetsumatsu(StrToInt(syyyy), strToInt(sMm)));
  end else begin
  	sDd2 := sDd;
  end;

  //すでにその月の消費税がある場合には削除してから
  //消費税額を再計算する。
  //削除のSql文
  //消費税の伝票の規則
  //Memo = '##消費税##'
  sSqlDel := 'DELETE FROM ' + CtblTDenpyou;
  sSqlDel := sSqlDel + ' WHERE ';
  sSqlDel := sSqlDel + 'Memo = ''' + CsCTAX + '''';
  sSqlDel := sSqlDel + ' AND ';
  sSqlDel := sSqlDel + ' DDate = ''' + sYyyy + '/' + sMm + '/' + sDd2 + '''';
  sSqlDel := sSqlDel + ' AND ';
  sSqlDel := sSqlDel + ' TokuisakiCode1 IN (' + sSql + ')';

  SB1.SimpleText := '消費税の計算中 （前消費税データの再計算）';
  SB1.Update;
  with frmDMMaster.QueryDelete do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSqlDel);
    ExecSql;
    Close;
  end;

  //消費税伝票の入力
	sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'SeikyuuSimebi = ' + sDd;
  if iTokuisakiCode1 = 0 then begin
  	;
  end else begin
	  sSql := sSql + ' AND ';
  	sSql := sSql + 'TokuisakiCode1 = ' + IntToStr(iTokuisakiCode1);
  end;

  sSql := sSql + ' AND ';
  sSql := sSql + 'TokuisakiCode1<50000';
  sSql := sSql + ' ORDER BY TokuisakiCode1';

  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
		  //消費税の伝票の入力
		  SB1.SimpleText := '消費税の計算中　' + FieldByName('TokuisakiCode1').AsString;
		  SB1.Update;
    	InsertCTax2(FieldByName('TokuisakiCode1').AsString, sYyyy + '/' + sMm + '/' + sDd2);
      Next;
    end;//of while

    Close;
  end;//of with

end;


//消費税の伝票の入力
//sYyyyMmDd -> 伝票日付
//
//1999/06/05
//  消費税の額が０の場合は伝票を挿入しない
//
//2005/10/11
//kaishuuHouhou が4以上のものは消費税の計算対象外にする
//1005/10/12
// 過去のデータの絡みがあるので上記の方法は危険
// kaishuuHouhou
procedure InsertCTax2(sTokuisakiCode1, sYyyyMmDd : String);
var
	sSqlIn, sSql, sYear, sMonth, sDay : String;
  sCTax, sSeikyuuShimebi, sGatsubun : String;
  dCTax : Double;
  wYyyy, wMm, wDd : Word;

  curUriageKingaku         : Currency; //2014.04.01 消費税対応
  douTax  :Double;    //2014.04.01 消費税対応
begin
  //請求締め日の１ヶ月前を取得
	DecodeDate(StrToDate(sYyyyMmDd), wYyyy, wMm, wDd);
  wDd := wDd +1;
  if wDd > 28 then begin //月末締めの場合
	  wDd := 1;
  end else begin
	  wMm := wMm - 1;
  	if wMm = 0 then begin
  		wMm := 12;
	    wYyyy := wYyyy - 1;
  	end;
  end;

	//消費税額の計算
  {
  sSql := 'SELECT TAXSUM = SUM(UriageKingaku) '; //2014.04.01 消費税対応  before
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  //sSql := sSql + ' TokuisakiCode1 =  ' + MyQuery.FieldByName('TokuisakiCode1').AsString;
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate Between ';
  sSql := sSql + '''' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sYyyyMmDd + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo <> ''' + CsCTAX + '''';

  sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 ';

  //2005.10.11
  //2004.10.12
  //sSql := sSql + ' AND ';
  //sSql := sSql + ' KaishuuHouhou <> 8';//雑収入=8
  //2006.01.10
  sSql := sSql + ' AND ';
  sSql := sSql + ' (KaishuuHouhou <> 8 or KaishuuHouhou is Null)';
}
  //2014.04.01 消費税対応  before
  {with frmDMMaster.QueryCTaxSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    dCTax := FieldByName('TAXSUM').AsFloat;


    //2004.3.31 前と以後で処理をかえる
    if StrToDate(sYyyyMmDd) >= StrToDate('2004/3/31') then begin
      //dCTax := Trunc(dCTax);
      //2004.04.02  切り捨てから四捨五入へ
      dCTax := (dCTax+0.01) * CsTaxRate;
      dCTax := Round(dCTax);
    end else begin
      dCTax := Trunc(dCTax * CsTaxRate);
    end;


    sCTax := FieldByName('TAXSUM').AsString;
    sCTax := Real2Str(dCTax, 0, 0);
    Close;
  end;
  }   //2014.04.01 消費税対応  before end

  //2014.04.01 消費税対応  after


  {
  with frmDMMaster.QueryCTaxSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;


        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;

      while not EOF do begin
        curUriageKingaku := FieldByName('UriageKingaku').AsFloat;
        if FieldbyName('DDate').AsString  < CsChangeTaxDate then begin
          douTax :=  CsOldTaxRate;
        end else begin
          douTax :=  CsTaxRate;
        end;
        dCTax      := dCTax + Round(curUriageKingaku * douTax); //2014.04.01 消費税対応

        next;
      end
  end;
}

  //2014.04.01 消費税対応
   sSql := 'SELECT';
   sSql := sSql + ' SUM(Shoukei) as Soukei,';
   sSql := sSql + ' SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)  as Tax';
   sSql := sSql + ' FROM ' + CtblTDenpyouDetail;
   sSql := sSql + ' WHERE TokuisakiCode1 =' + '''' + sTokuisakiCode1 + '''';
   sSql := sSql + ' AND NouhinDate >= ' + '''' + DateToStr(EncodeDate(wYyyy, wMm, wDd)) + '''';
   sSql := sSql + ' AND NouhinDate <= ' + '''' + sYyyyMmDd + '''';
   sSql := sSql + ' GROUP BY TokuisakiCode1';

  with frmDMMaster.QueryCTaxSum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;


     dCTax := FieldByName('Tax').AsFloat;

  end;

  sCTax := Real2Str(dCTax, 0, 0);

  //2014.04.01 消費税対応  after end

  //月分の計算
  //1999/09/14
	// TDenpyouのGatsubun に何月分の売上かを入力する
  //伝票日付が締め日を越えていたら月を１進める
  sSeikyuuShimebi := MTokuisaki.GetShimebi(sTokuisakiCode1);
  sGatsubun := sYyyyMmDd;
 	sYear := Copy(sGatsubun, 1, 4);
 	sMonth := Copy(sGatsubun, 6, 2);
 	sDay := Copy(sGatsubun, 9, 2);
  if sSeikyuuShimebi = '0' then begin //月末ならそのまま
    sGatsubun := sYear + '/' + sMonth + '/01';
  end else begin
    if StrToInt(sDay) > StrToInt(sSeikyuuShimebi) then begin
    	if StrToInt(sMonth) = 12 then begin
        sMonth := '01';
      end else begin
      	sMonth := IntToStr(StrToInt(sMonth) + 1);
      end;
    end;
    sGatsubun := sYear + '/' + sMonth + '/01';
  end;


  //消費税の額がなければ何もしない
  if (sCTax = '') or (sCTax = '0')then begin
		Exit;
  end else begin
		sSqlIn := 'INSERT INTO ' + CtblTDenpyou;
	  sSqlIn := sSqlIn + '(';
		sSqlIn := sSqlIn + 'TokuisakiCode1, ';
	  sSqlIn := sSqlIn + 'TokuisakiCode2, ';
		//sSqlIn := sSqlIn + 'DNumber, ';
		sSqlIn := sSqlIn + 'InputDate, ';
		sSqlIn := sSqlIn + 'DDate, ';
	  sSqlIn := sSqlIn + 'UriageKingaku, ';
	  sSqlIn := sSqlIn + 'Gatsubun, '; //1999/09/14
		sSqlIn := sSqlIn + 'Memo';
	  //sSqlIn := sSqlIn + 'KaishuuHouhou ';
		sSqlIn := sSqlIn + ') Values (';
		sSqlIn := sSqlIn + sTokuisakiCode1 + ',';
		sSqlIn := sSqlIn + MTokuisaki.GetTokuisakiCode2(sTokuisakiCode1) + ',';
		//sSqlIn := sSqlIn + 'DNumber, ';
		sSqlIn := sSqlIn + '''' + DateToStr(Date()) + ''', ';
		sSqlIn := sSqlIn + '''' + sYyyyMmDd + ''', ';
	  sSqlIn := sSqlIn + Real2Str(Str2Real(sCTax), 0,0) + ', ';
		sSqlIn := sSqlIn + '''' + sGatsubun + ''', ';
		sSqlIn := sSqlIn + '''' + CsCTAX + '''';
	  //sSqlIn := sSqlIn + 'KaishuuHouhou ';

		sSqlIn := sSqlIn + ')';
	  with frmDMMaster.QueryInsert do begin
  		Close;
  	  Sql.Clear;
	    Sql.Add(sSqlIn);
    	ExecSql;
  	  Close;
	  end;//of with
  end;//of if
end;


//請求期間の取得
//1999/05/04
{
Function TfrmPBill.GetBillTarm(sTokuisakiCode1:String; Var sDateFrom, sDateTo : String) : Boolean;
var
	sSql, sShiharaitsuki, sShimebi, sYyyy, sMm : String;
  iMm, iYyyy : Integer;
  iShimebi : Integer;
begin
	//締め日関連情報の取得
  iYyyy := StrToInt(CBYyyy.Text);
  iMm := StrToInt(CbMm.Text);

	//得意先コードから締め日を取得する
	iShimebi := GetShimebi(sTokuisakiCode1);
  if iShimebi = 99 then begin
  	ShowMessage('得意先コード' + sTokuisakiCode1 + 'が見つかりません');
    Exit;
  end;
  case iShimebi of
  0 :begin //月末
       sDateFrom := sYyyy + '/' + sMm + '/' + '1';
       sDateTo   := sYyyy + '/' + sMm + '/' + IntToStr(HKLib.GetGetsumatsu(iYyyy, iMm));
     end;
  10:begin
       sDateFrom := sYyyy + '/' + sMm + '/' + '11';
       //翌月の
       iMm := iMm + 1;
       if iMm = 13 then begin
       	iMm := 1;
        iYyyy := iYyyy + 1;
       end;
       sDateTo   := sYyyy + '/' + sMm + '/10';
     end;
  20:begin
       sDateFrom := sYyyy + '/' + sMm + '/' + '21';
       //翌月の
       iMm := iMm + 1;
       if iMm = 13 then begin
       	iMm := 1;
        iYyyy := iYyyy + 1;
       end;
       sDateTo   := sYyyy + '/' + sMm + '/20';
     end;

  end;//of case

end;
}


//得意先コードから支払い月を取得する
//0 当月
//1 翌月
//2 翌々月
//3 翌翌々月
function GetShiharaitsuki(sTokuisakiCode1:String) : String;
var
	sSql: String;
begin
	sSql := 'SELECT ShiharaibiM ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with frmDMMaster.QueryShiharaibiM do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('ShiharaibiM').AsString;
    end else begin
    	Result := '';
    end;//of if
  	Close;
  end;
end;


//得意先コードから締め日を取得する
//5  日
//10 日
//15 日
//20 日
//25 日
//0  月末
function GetShimebi(sTokuisakiCode1:String) : Integer;
var
	sSql: String;
begin
	sSql := 'SELECT SeikyuuSimebi ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
	sSql := sSql + ' TokuisakiCode1 = ' + sTokuisakiCode1;
  with frmDMMaster.QueryShiharaibiM do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
    	Result := FieldByName('SeikyuuSimebi').AsInteger;
    end else begin
    	Result := 99;
    end;//of if
  	Close;
  end;
end;


//前月請求残高の取得
//前月分までの売上金額（税抜き）と本日までの回収（税込み）の差額
Function GetRuikeiZandaka(sTokuisakiCode1, sDateFrom, sDateTo:String):Integer;
var
	sSql : String;
  iSum, iSum2, iSum3 : Integer;
  wYyyy,wMm,wDd:word;
begin

	//前月までの累計金額
  //1999/07/25 返品に対応
  sSql := 'SELECT UriageSum = Sum(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';
	sSql := sSql + 'DDate <= ''' + DateToStr(StrToDate(sDateFrom)-1) + '''';

	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku > 0';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;

	//1999/09/25
		sSql := sSql + ' AND ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) = 0 '; //返金でない

  with frmDMMAster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //請求締め日までの回収金額
  sSql := 'SELECT UriageSum = Sum(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';

	sSql := sSql + ' ( ';

	sSql := sSql + 'DDate <= ''' + sDateTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku < 0';
  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'')))' ;

  //1999/08/09
 	sSql := sSql + ' OR ';
  sSql := sSql + ' PATINDEX(''%返金%'', Memo) > 0 ';
	sSql := sSql + ' ) ';

  with frmDMMAster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum2 := FieldByName('UriageSum').AsInteger;
    Close;
  end;

  //added 2001/10/12
  //消費税のマイナス分を引く
  //ラフアンドレディー対策
  sSql := 'SELECT TaxSum = Sum(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';

	sSql := sSql + 'TokuisakiCode1 = ' + sTokuisakiCode1;
	sSql := sSql + ' AND ';

	sSql := sSql + ' ( ';

  sDateTo := DateToStr(IncMonth(strtodate(sDateTo),-1));
	sSql := sSql + 'DDate <= ''' + sDateTo + '''';
	sSql := sSql + ' AND ';
	//sSql := sSql + ' UriageKingaku < 0';
  sSql := sSql + ' ((UriageKingaku < 0) AND (PATINDEX(''%#消費税#%'', Memo) > 0))' ;

	sSql := sSql + ' ) ';

  with frmDMMAster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    iSum3 := FieldByName('TaxSum').AsInteger;
    Close;
  end;

  iSum := iSum + iSum2;
  //2005.10.10
  //hkubota 消費税のマイナスは今後考慮する必要なし？？
  //Result := iSum - iSum3;
  Result := iSum;
end;


procedure TfrmPBill.EditTokuisakiCode1Exit(Sender: TObject);
begin
	{
  if Length(EditTokuisakiCode1.Text) > 0 then begin
  	ShowMessage(EditTokuisakiCode1.Text + 'の請求書を発行します');
  end else begin
  	Exit;
  end;
  LabelTokuisakiName.Caption := GetTokuisakiName(EditTokuisakiCode1.Text);
	}
  //得意先コードの締め日を取得する
  //CBDd.Text := IntToStr(GetShimebi(EditTokuisakiCode1.Text));
  if CBDd.Text = '99' then begin
  	ShowMessage('得意先コードが見つかりません');
    CBDd.Text := '0';
    Exit;
  end;
end;


//期間限定売上金額の合計を求めるSQl文を作る
function MakeSqlUriageSum(sTokuisakiCode1, sDateFrom, sDateTo : String) : String;
var
	sSql : String;
begin
	//消費税額の計算
  sSql := 'SELECT TAXSUM = SUM(UriageKingaku) ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate Between ';
  sSql := sSql + '''' + sDateFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sDateTo + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' ((UriageKingaku > 0) OR (SUBSTRING(Memo, 1, 2) IN (''返品'', ''値引'')))' ;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo <> ''' + CsCTAX + '''';

	Result := sSql;
end;

//2014.04.01 消費税対応 beggn
function GetTokuisakiTax(sTokuisakiCode1, sGatsubun : String) : String;
var
	sSql : String;
  sTax : String;
begin
	//消費税額の計算
  sSql := 'SELECT UriageKingaku ';
  sSql := sSql + ' FROM ' + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' Gatsubun = ''' + sGatsubun + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo = ''' + CsCTAX + '''';

  with frmDMMAster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sTax := FieldByName('UriageKingaku').AsString;
    Close;
  end;

  Result := sTax;

end;
//2014.04.01 消費税対応 end


procedure TfrmPBill.CheckBox1Click(Sender: TObject);
begin
	//請求対象期間を指定する
	if CheckBox1.Checked = True then begin
  	if EditTokuisakiCode1.Text = '' then begin
	  	ShowMessage('得意先コードを指定してください');
      EditTokuisakiCode1.Setfocus;
      Exit;
    end else begin
	  	ShowMessage(EditTokuisakiCode1.Text + 'の回収期間を指定します');
    end;
  end;

end;


procedure TfrmPBill.BitBtn4Click(Sender: TObject);
var
	i, iNengajou : Integer;
  sLine, sSql, sYuubin, sAdd1, sAdd2, sAdd3, sName : String;
  sAddPre, sTokuisakiCode1, sTokuisakiCode2, sYomi,sHeitenFlag : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('請求書用タックシールを出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' SeikyuushoHassouSaki>0 ';

  //テスト用
  //sSql := sSql + ' AND TokuisakiCode1 < 100 ';

  //2000/11/20 Added
  sSql := sSql + ' AND HeitenFlag <> 1 ';

  //得意先コード指定
  if Length(EditTokuisakiCode1.Text) > 0 then begin
	  sSql := sSql + ' AND TokuisakiCode1 in (' + EditTokuisakiCode1.Text + ')';
  end;


//  sSql := sSql + ' ORDER BY TokuisakiCode1';
  sSql := sSql + ' ORDER BY ChainCode';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Nengajou);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    while not EOF do begin
    	iNengajou := FieldByName('SeikyuushoHassouSaki').AsInteger;
      //  sYuubin, sAdd1, sAdd2, sName : String;
      sName := '';
    	sYuubin := '';
			sAdd1   := '';
      sAdd2   := '';
      sAdd3   := '';
      sName   := '';
      if iNengajou = 1 then begin  //会社
      	sYuubin := FieldByName('ZipOffice').AsString;
	    	sAdd1   := FieldByName('Add1Office').AsString;
	    	sAdd2   := FieldByName('Add2Office').AsString;
	    	sAdd3   := FieldByName('Add3Office').AsString;
        sName   := FieldByName('AtenaOffice').AsString;
      end else if iNengajou = 2 then begin //店
      	sYuubin := FieldByName('Zip').AsString;
	    	sAdd1   := FieldByName('Add1').AsString;
	    	sAdd2   := FieldByName('Add2').AsString;
	    	sAdd3   := FieldByName('Add3').AsString;
        sName   := FieldByName('TokuisakiName').AsString;
      end else if iNengajou = 3 then begin //自宅
      	sYuubin := FieldByName('ZipHome').AsString;
	    	sAdd1   := FieldByName('Add1Home').AsString;
	    	sAdd2   := FieldByName('Add2Home').AsString;
	    	sAdd3   := FieldByName('Add3Home').AsString;
        sName   := FieldByName('AtenaHome').AsString;
      end else begin
	    	ShowMessage(FieldByName('TokuisakiCode1').AsString+'のデータが不完全です');

      end;
      //2000/11/20 Added
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sYomi := FieldByName('TokuisakiNameYomi').AsString;
      sHeitenFlag := FieldByName('HeitenFlag').AsString;

      //発送先によりかえる
      if iNengajou = 1 then begin  //会社
	      sName := sName + '　';
      end else if iNengajou = 2 then begin //店
	      //sNameの整形
  	    //半角スペース以降を表示
			  if Pos(' ', sName) > 0 then begin
      		sName := Copy(sName, Pos(' ', sName)+1, Length(sName));
     	 	end else if Pos('　', sName) > 0 then begin
      		sName := Copy(sName, Pos('　', sName)+2, Length(sName));
      	end;
	      sName := sName + '　御中';
      end else if iNengajou = 3 then begin //自宅
	      sName := sName + '　様';
      end else begin
         ;
      end;

      //ファイルに書き込み
    	sLine := sYuubin;
    	sLine := sLine + ',' + sAdd2 + ',' + sAdd3;
    	sLine := sLine + ',' + sName;

      //2000/11/20 Added
    	sLine := sLine + ',' + sTokuisakiCode1;
    	sLine := sLine + ',' + sTokuisakiCode2;
    	sLine := sLine + ',' + sYomi;

      //2001/01/28 Added
    	sLine := sLine + ',' + FieldByName('Ka').asString;
    	sLine := sLine + ',' + FieldByName('TantoushaCode').asString;
    	sLine := sLine + ',' + FieldByName('ChainCode').asString;
    	sLine := sLine + ',' + FieldByName('SeikyuuSimebi').asString;

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      //If Address is same then do not write.
      if (sAddPre = (sAdd2 + sAdd3)) and (Length(sAdd2 + sAdd3)>0) then begin
      	;
      end else begin
		  	HMakeFile(CFileName_Nengajou, sLine);
      end;
      sAddPre := sAdd2+sAdd3;
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '鷹松屋宛名ラベル.xls', '', SW_SHOW);

end;

//請求明細印刷ボタンがクリックされた
procedure TfrmPBill.BitBtn5Click(Sender: TObject);
var
strTokuisakiCode1      : String;
strTokuisakiCode1Tmp   : String;
LArrayOfTokuisakiCode1 : array[0..9999] of String;
i                      : Integer;
strShimebi             : String;
sSql, sWhere           : String;
wYyyy,wMm,wDd          : word;
strShimeDate           : String;
strStartDate           : String;
strEndDate             : String;

dlg: TForm;
res: Word;
begin
//確認メッセージを出す
//2002/07/02 Added by H.Kubota
dlg := CreateMessageDialog('請求明細書を印刷しますか', mtInformation,
                             [mbYes, mbNo]);
dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
res := dlg.ShowModal;
if res <> mrYes then begin
  exit;
end;
dlg.Free;



// 取引先コード１，開始日，終了日をDenpyouPrintSeikyuuに渡す．
// この際，4通りの指定方法がある．
//　　�@ 得意先コードをカンマ区切りで複数指定
//　　�A 得意先コードを複数指定＆回収対象帰還指定
//　　�B 締日指定
//    �C 締日指定＆回収対象帰還指定

 //
 // 取引先コード１のリストを取得
 //

 // 得意先コードを指定している場合
 if EditTokuisakiCode1.Text <> '' then begin

   strTokuisakiCode1Tmp := EditTokuisakiCode1.Text + ',';

   i := 0;
   while strTokuisakiCode1Tmp <> '' do begin
	   LArrayOfTokuisakiCode1[i] := Copy(strTokuisakiCode1Tmp, 1, Pos(',', strTokuisakiCode1Tmp)-1);
     strTokuisakiCode1Tmp := Copy(strTokuisakiCode1Tmp, (Pos(',', strTokuisakiCode1Tmp)+1), Length(strTokuisakiCode1Tmp));
     i := i + 1;
   end;//of while

 end

 // 得意先コードでなく，締日を指定している場合
 else begin

   strShimebi := CBDd.Text;

   sSql := 'SELECT ic=Count(TokuisakiCode1) FROM ' + CtblMTokuisaki;
   sWhere := ' WHERE SeikyuuSimebi = ' + '''' + strShimebi + '''';
   sWhere := sWhere + ' and TokuisakiCode1<50000 ';

   sWhere := sWhere + ' and SeikyuushoFlag <> 1 '; // add 2015.09.01



   //Added by Hajime Kubota 2003.01.06
   //
   if Length(EditTantoushaCode.Text)>0 then begin
	   sWhere := sWhere + ' and TantoushaCode=' + EditTantoushaCode.Text;
   end;
   if Length(EditShuukeiCode.Text)>0 then begin
	   sWhere := sWhere + ' and ShuukeiCode=' + EditShuukeiCode.Text;
   end;
   if Length(EditTokuisakiCode2.Text)>0 then begin
	   sWhere := sWhere + ' and TokuisakiCode2=' + EditTokuisakiCode2.Text;
   end;

   sSql := sSql + sWhere;
   with qrySeikyuu do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    SB1.SimpleText := FieldbyName('ic').AsString;
    if SB1.SimpleText = '0' then begin
    	ShowMessage('該当する得意先はありません'+sSql);
      exit;
    end;
    Close;
   end; //

   sSql := 'SELECT TokuisakiCode1 FROM ' + CtblMTokuisaki;
   sSql := sSql + sWhere;
   //2002.07.31
   //   sSql := sSql + ' ORDER BY TokuisakiCode1';

   //sSql := sSql + ' and TokuisakiCode1<50000 ';
   //sSql := sSql + ' and TokuisakiCode2 = 23 ';

   {
   sSql := sSql + ' ORDER BY TokuisakiCode2';
   sSql := sSql + ', TantoushaCode, Ka, TokuisakiNameYomi';
   }
   //2002.10.31 並び替えを変更
   //郵送持参(TantoushaCode)、集計コード(ShuukeiCode)、得意先コード２、よみ
   sSql := sSql + ' ORDER BY TantoushaCode desc, ShuukeiCode, TokuisakiCode2';
   sSql := sSql + ', TokuisakiNameYomi';

    SB1.SimpleText := '対象得意先リスト作成中';
    SB1.Update;
   with qrySeikyuu do begin
   	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('該当する得意先がありません');
        Close;
        Exit;
      end;

    // 得意先コードのリスト設定
    i := 0;
    while not EOF do begin
      LArrayOfTokuisakiCode1[i] := FieldbyName('TokuisakiCode1').AsString;
      SB1.SimpleText := intToStr(i);
      SB1.Update;
      i := i+1;
      next
    end;

    Close;
   end;

 end;//of if

 //
 // 得意先コードでループして，DenpyouPrintSeikyuuをコール
 //

 i := 0;
 while LArrayOfTokuisakiCode1[i] <> '' do begin
  SB1.SimpleText := IntTostr(i) + '件目 得意先コード=' + LArrayOfTokuisakiCode1[i];
  SB1.Update;
  // �@ 得意先コード設定
  strTokuisakiCode1 := LArrayOfTokuisakiCode1[i];

  // �A 締日取得
  strShimebi        := IntToStr(GetShimebi(strTokuisakiCode1));
  if strShimebi = '0' then begin //締め日が月末
    wDd := GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text));
  end
  else begin
    wDd := StrToint(strShimebi);
  end;
  strShimeDate := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(wDd);

  // �B 終了日設定
   // 回収対象期間を指定している場合は，それを終了日とする．
   if CheckBox1.Checked = True then begin
     DateTimeToString(strEndDate, 'yyyy/mm/dd', DTPTo.Date);
   end
   // 回収対象帰還を指定しない場合は，締日＝最終日とする．
   else begin
     strEndDate := strShimeDate;
   end;

  // �C 開始日設定
  DecodeDate(StrToDate(strShimeDate), wYyyy, wMm, wDd);
  if strShimebi = '0' then begin //締め日が月末
	  wDd := 1;
  end else begin
		wMm := wMm - 1;
	  if wMm = 0 then begin
  		wMm := 12;
    	wYyyy := wYyyy - 1;
	  end;
	  wDd := wDd + 1;
	end;
  strStartDate := DateToStr(EncodeDate(wYyyy, wMm, wDd));

  // グローバル変数へ値をセット
	GTokuisakiCode1 := strTokuisakiCode1;
  GStartDate      := strStartDate;
	GEndDate        := strEndDate;

  // DenpyouPrintSeikyuu呼出
  TfrmDenpyouPrintSeikyuu.Create(Self);

  i := i + 1;
 end;

 ShowMessage('印刷が完了しました');

end;


end.
