unit PCourceUriage2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, ShellAPI;

type
  TfrmPCourceUriage2 = class(TfrmMaster)
    DTP1: TDateTimePicker;
    Label3: TLabel;
    DTP2: TDateTimePicker;
    Label4: TLabel;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmPCourceUriage2: TfrmPCourceUriage2;

implementation

uses DMMaster, Inter, HKLib, PasswordDlg;
                                
{$R *.DFM}

//エクセルへ出力ボタンがクリックされた 
procedure TfrmPCourceUriage2.BitBtn2Click(Sender: TObject);
var
	sLine, sSql, sTokuisakiCode1, sTokuisakiCode2, sTokuisakiname, sUriageKingaku, sDateFrom, sDateTo, sShoukei : string;
 	F : TextFile;
  iSum, iSum2, iZanOnOff, iGenka, iShoukeii, iArari, iArariRitsu : Integer;
  dArari, dArariRitsu : Double;
  wYyyy, wMm, wDd : Word;
  sShuukinCode : String;
  rGenka, rShoukeii, rArari, rArariRitsu : Real;
  sGenka, sShoukeii, sArari, sArariRitsu : String;
  sChainCode : String;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション
  sSql := 'SELECT T.TokuisakiCode1, T.TokuisakiCode2,ChainCode';
  sSql := sSql + ', T.TokuisakiName ';
  sSql := sSql + ', T.ShuukinCode '; //2004.04.16 Added H.K.
  sSql := sSql + ', SUM(DD.Genka * DD.Suuryou) Genka ';
  sSql := sSql + ', SUM(DD.Shoukei) Shoukeii,DenpyouCode';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblMTokuisaki + ' T';
  sSql := sSql + ' INNER JOIN ' + CtblTDenpyouDetail + ' DD';
  sSql := sSql + ' ON DD.TokuisakiCode1 = T.TokuisakiCode1 ';
  sSql := sSql + ' INNER JOIN ' + CtblMItem + ' MI ';
  sSql := sSql + ' ON DD.Code1 = MI.Code1 AND DD.Code2 = MI.Code2';
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' DD.NouhinDate >=  ''' + DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' DD.NouhinDate <=  ''' + DateToStr(DTP2.Date) + '''';
  sSql := sSql + ' GROUP BY T.TokuisakiCode1, T.TokuisakiCode2, T.TokuisakiName , T.ShuukinCode, DD.DenpyouCode,T.ChainCode ';
  sSql := sSql + ' ORDER BY ';
  sSql := sSql + ' T.TokuisakiCode2, T.TokuisakiCode1';


	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_CourceUri);
  Rewrite(F);
	CloseFile(F);

  //2000/03/18
  //まず日付を出力する
  sLine := DateToStr(DTP1.Date) + ' 〜 ' + DateToStr(DTP2.Date);
 	HMakeFile(CFileName_CourceUri, sLine);
  //タイトルを出力する
  sLine := '得意先コード２';
  sLine := sline + ',' + '得意先コード１';
  sLine := sline + ',' + 'チェーンコード';  // add 2015.09.01 by utada
  sLine := sline + ',' + '集金コード';
  sLine := sline + ',' + '得意先名';
  sLine := sline + ',' + '売上金額';
  sLine := sline + ',' + '原価';
  sLine := sline + ',' + '粗利';
  sLine := sline + ',' + '粗利率(%)';
 	HMakeFile(CFileName_CourceUri, sLine);

	iArari := 0;
	iArariRitsu := 0;
  with frmDMMaster.QueryMTokuisaki do begin
		Close;
    Sql.Clear;
   	Sql.Add(sSql);
	  open;
    while not EOF do begin
		SB1.SimpleText := '計算中 -> '+FieldByName('TokuisakiCode1').AsString;
		Update;
		sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
		sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
    sChainCode      := FieldByName('ChainCode').AsString;     // add 2015.09.01 by utada
		sShuukinCode    := FieldByName('ShuukinCode').AsString;
		sTokuisakiName := FieldByName('TokuisakiName').AsString;
		//sUriageKingaku := FieldByName('UriageKingaku').AsString;
                         
		iGenka        := FieldByName('Genka').AsInteger;
		rShoukeii     := FieldByName('Shoukeii').AsFloat;

		sShoukeii		:= Real2Str(rShoukeii, 0, 0);
		
		
		iArari := StrToInt(sShoukeii) - iGenka;


    sArariRitsu  := '';
    if(StrToInt(sShoukeii) <> 0) then begin
       dArariRitsu :=  ( iArari / StrToInt(sShoukeii) ) * 100;
       sArariRitsu := FormatFloat('0',dArariRitsu) + '%';

    end;

		sLine := sTokuisakiCode2 + ', ' + sTokuisakiCode1 + ',' + sChainCode + ',' + sShuukinCode;
		sLine := sLine + ',' + sTokuisakiName + ', ' + sShoukeii;
    sLine := sLine + ', ' + IntToStr(iGenka) + ', ' + IntToStr(iArari) + ', ' + sArariRitsu;
 		HMakeFile(CFileName_CourceUri, sLine);
     	Next;
    end;//of while
    Close;
  end;//of with

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + 'コース別売上2.xls', '', SW_SHOW);
end;

procedure TfrmPCourceUriage2.FormCreate(Sender: TObject);
begin
  inherited;
	Height := 200;
  Width := 580;
//  DTP1.Date := Date - 30;
  DTP1.Date := Date;
  DTP2.Date := Date;

  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin
    ;
  end else begin
    Exit;
  end;
  if GPassWord = CPassword0 then begin
    Beep;
  //add2009.04.08 uta
  end else begin
    //ShowMessage('PassWordが違います');
    close;
    GPassWord := '';
    exit;
  end;
  GPassWord := '';


end;

end.
