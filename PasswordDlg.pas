unit PasswordDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TdlgPasswordDlg = class(TForm)
    Label1: TLabel;
    Password: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure OKBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  dlgPasswordDlg: TdlgPasswordDlg;

implementation
uses
	inter;
{$R *.DFM}


procedure TdlgPasswordDlg.OKBtnClick(Sender: TObject);
begin
	GPassWord := Password.text;
{
	if GPassWord = CPassword1 then begin
    Beep;
  end else begin
  	ShowMessage('PassWordが違います');
    exit;
  end;
}
  //dlgPasswordDlg.Close;
end;

procedure TdlgPasswordDlg.FormActivate(Sender: TObject);
begin
	Password.text := '';
end;

procedure TdlgPasswordDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

end.

