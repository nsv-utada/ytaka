unit ShiharaiNyuuryoku;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Mask, DB,
  DBTables;

type
  TfrmShiharaiNyuuryoku = class(TfrmMaster)
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    CBName: TComboBox;
    EditYomi: TEdit;
    CBShiiresakiCode: TComboBox;
    CBShiharai: TCheckBox;
    CBTax: TCheckBox;
    DTP1: TDateTimePicker;
    Label4: TLabel;
    Label7: TLabel;
    EditKingaku: TEdit;
    Label20: TLabel;
    EditGatsubun: TMaskEdit;
    Label14: TLabel;
    Memo1: TMemo;
    labelZan4: TLabel;
    EditZandaka: TMemo;
    Query1: TQuery;
    QueryInShiire: TQuery;
    QueryINShiireDetail: TQuery;
    Label8: TLabel;
    CBNyukinStatus: TComboBox;
    Label9: TLabel;
    QueryShimebi: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure CBShiiresakiCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CBTaxClick(Sender: TObject);
    procedure CBShiharaiClick(Sender: TObject);
    procedure DTP1Exit(Sender: TObject);
    procedure CBTaxExit(Sender: TObject);
    procedure CBShiharaiExit(Sender: TObject);
    procedure EditGatsubunExit(Sender: TObject);
  private
    { Private declarations }
    procedure MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
    procedure MakeForm(sCode:String);
    procedure IntblMShiire;
    procedure IntblMShiireDetail;
    function MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String):String;
    Function GetShimebi(sCode:String) : Integer;


  public
    { Public declarations }
  end;

var
  frmShiharaiNyuuryoku: TfrmShiharaiNyuuryoku;

implementation
uses
  Inter, DM1, PasswordDlg, DMMaster,HKLib;

{$R *.dfm}

procedure TfrmShiharaiNyuuryoku.FormCreate(Sender: TObject);
var
 sYyyy,sMm : String;
begin
  inherited;
  top  := 5;
  Left := 5;
  Width := 488;
  Height := 355;
  DTP1.Date := Date();
  sYyyy := Copy(DateToStr(Date()),0,4);
  sMm := Copy(DateToStr(Date()),6,2);
  if length(sMm) = 1 then sMm := '0' + sMm;
  EditGatsubun.Text := sYyyy + '/' + sMm;

end;

procedure TfrmShiharaiNyuuryoku.CBShiiresakiCodeKeyDown(Sender: TObject;
var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    MakeCBShiiresakiName('Code', CBShiiresakiCode.Text, 2);
  end;
end;

procedure TfrmShiharaiNyuuryoku.EditYomiKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    MakeCBShiiresakiName('Yomi', EditYomi.Text, 1);
  end;
end;

procedure TfrmShiharaiNyuuryoku.CBNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key=VK_F1) then begin
    MakeCBShiiresakiName('Name', CBName.Text, 1);
  end;

end;

procedure TfrmShiharaiNyuuryoku.CBNameExit(Sender: TObject);
var
 sCode : String;
begin
  //Codeを取得して仕入先を検索する
  sCode := Copy(CBName.Text, 1, (pos(',', CBName.Text)-1));
  if length(sCode) > 0 then begin
    MakeForm(sCode);
  end;
end;

Function TfrmShiharaiNyuuryoku.GetShimebi(sCode:String) : Integer;
var
  sSql,sShimebi : String;
  iShimebi : Integer;
begin
  sSql := 'Select Shimebi from ' + CtblMShiiresaki;
  sSql := sSql +  ' where ';
  sSql := sSql + ' Code = ' + sCode ;
  with QueryShimebi do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
       sShimebi := FieldByName('Shimebi').AsString;
    end;
    close;
  end;
  if sShimebi = '' then begin
   sShimebi := '0';
  end;
  iShimebi := StrToInt(sShimebi);
  result := iShimebi;
end;

//Codeからフォームを表示する
procedure TfrmShiharaiNyuuryoku.MakeForm(sCode:String);
var
  sTmp, sSql,strYoubiShiteiNouhin : String;
  i : Integer;
  iShimebi : Integer;
  sFrom, sTo : String;
  iDd,  iMm,  iYyyy : Integer;
  iDd2, iMm2, iYyyy2 : Integer;

begin
  //EditGatsubun.Text 月分
  //〆日の翌日から〆日まで
  // 2006/04  2006/04/01-2006/04/30
  // 2006/04  2006/03/21-2006/04/20
  iYyyy := StrToInt(Copy(EditGatsubun.Text, 0 ,4));
  sTmp := Trim(Copy(EditGatsubun.Text, 6 ,2));
  iMm   := StrToInt(sTmp);
  //仕入先の〆日を求める
  iShimebi := GetShimebi(sCode);
  iDd := HKLib.GetGetsumatsu(iYyyy, iMm);
  if iShimebi = 0 then begin
    sFrom := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/01';
    sTo   := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/' +IntToStr(iDd);
  end else begin
    iDd := iShimebi;        //To
    iDd2  := iShimebi + 1;  //From
    iMm2 := iMm - 1;        //From
    if iMm2 = 0 then begin
      iMm2 := 12;
      iYyyy2 := iYyyy-1;
    end else begin
      iYyyy2 := iYyyy;
    end;
    sFrom := IntToStr(iYyyy2) + '/' + IntToStr(iMm2) + '/' + IntToStr(iDd2);
    sTo   := IntToStr(iYyyy)  + '/' + IntToStr(iMm)  + '/' + IntToStr(iDd);
  end;

  sSql := 'SELECT S=Sum(Shoukei) FROM ' + CtblTShiireDetail;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' ShiiresakiCode = ' + sCode;
  sSql := sSql + ' AND ';
  sSql := sSql + ' NouhinDate between  ''' + sFrom + '''';
  sSql := sSql + ' AND ''' + sTo + '''';

  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
       CBShiiresakiCode.Text := sCode;
       EditZandaka.Text := FieldByName('S').AsString;
    end;
    close;
  end;
end;


{
	仕入先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmShiharaiNyuuryoku.MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;

  sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  //sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('Code').AsString + ',' + FieldByName('Name').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBName.DroppedDown := True;
end;

//登録ボタンがクリックされた
procedure TfrmShiharaiNyuuryoku.BitBtn2Click(Sender: TObject);
begin
  //確認メッセージ
  if MessageDlg('登録しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  IntblMShiire;
  IntblMShiireDetail;

  Showmessage('登録しました。');

end;
procedure TfrmShiharaiNyuuryoku.IntblMShiire;
var
  sSql : String;
begin
{
  sSql := 'INSERT INTO ' + CtblTShiire;
  sSql := sSql + ' (';
  sSql := sSql + ' ShiiresakiCode ';
  sSql := sSql + ' ,SDenpyouCode';
  sSql := sSql + ' ,Gatsubun';
  sSql := sSql + ' ,InputDate';
  sSql := sSql + ' ,DDate';
  sSql := sSql + ' ,DenpyouKingaku';
  sSql := sSql + ' ,Tax';
  sSql := sSql + ' ,Memo';
  sSql := sSql + ' ,Flag';
  sSql := sSql + ' ,NyukinStatus';
  sSql := sSql + ' ,Nyuuryokusha';
  sSql := sSql + ' ,Bikou';
  sSql := sSql + ' ,Name2';
  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + ' (';
  sSql := sSql + '' + CBShiiresakiCode.Text     + ',' ;
  sSql := sSql + '''' +  + ''',' ; //伝票コード
  sSql := sSql + '''' +  + ''',' ;
  sSql := sSql + ')';


  with QueryINShiire do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;//of with
}
end;

//支払い入力の伝票番号作成のロジック
// 支払い      ShiireShiharaiFlg = 1
// 消費税調整  ShiireShiharaiFlg = 3
//  X003YyyyMmDd01
function TfrmShiharaiNyuuryoku.MakeSDenpyouCode(iShiiresakiCode:integer; sNyuukabi:String):String;
var
  sYyyy, sMm, sDd, sDenpyouCode : String;
  sNum, sYyMmDd, sYyMmDd2, sShiiresakiCode, sSql, sTmp:String;
  i, inum : Integer;
begin
  sYyyy := Copy(sNyuukabi,0,4);
  sMm := Copy(sNyuukabi,6,2);
  sDd := Copy(sNyuukabi,9,2);
  sNyuukabi := sYyyy + sMm + sDd;

  sShiiresakiCode := IntToStr(iShiiresakiCode);
  if length(sShiiresakiCode)=1 then begin
    sShiiresakiCode := '00' + sShiiresakiCode;
  end else if length(sShiiresakiCode)=2 then begin
    sShiiresakiCode := '0' + sShiiresakiCode;
  end;

  sSql := 'Select m=MAX(SDenpyouCode) from ' + CtblTShiireDetail;
  sSql := sSql + ' where ShiiresakiCode=' +  sShiiresakiCode;
  sSql := sSql + ' and NouhinDate =''' +  DateToStr(DTP1.Date) + '''';
  sSql := sSql + ' and (ShiireShiharaiFlg = 1 ';
  sSql := sSql + ' or  ShiireShiharaiFlg = 3) ';

  with Query1 do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
    sTmp := FieldByName('m').asString;
    Close;
  end;
  if sTmp='' then begin
    sDenpyouCode := sShiiresakiCode + sNyuukabi + '01';
  end else begin
    //この部分は、仕入れ入力のロジックと違うので注意
    sYyMmDd := Copy(sTmp, 5, 8);
    sNum    := Copy(sTmp, 13, 2);
    iNum := StrToInt(sNum) + 1;
    if iNum > 99 then begin
      ShowMessage('伝票番号がオーバーしました。管理者に連絡してください');
      exit;
    end;
    sNum := IntToStr(iNum);
    if length(sNum) = 1 then begin
      sNum := '0' + sNum;
    end;
    sDenpyouCode := sShiiresakiCode + sNyuukabi + sNum;
  end;//of if
    Result := 'X' + sDenpyouCode;
end;

procedure TfrmShiharaiNyuuryoku.IntblMShiireDetail;
var
  sShiireShiharaiFlg, sSql, sDenpyouCode, sDate : String;
  sShiharaiHouhou : String;
  cKingaku : Currency;
begin
  //伝票コード
{
  sDenpyouCode := CBShiiresakiCode.Text;
  if Length(sDenpyouCode) = 1 then begin
    sDenpyouCode := '00' + sDenpyouCode;
  end else if Length(sDenpyouCode) = 2 then begin
    sDenpyouCode := '0' + sDenpyouCode;
  end;
  sDate := DateToStr(DTP1.Date);
  sDenpyouCode := sDenpyouCode + Copy(sDate,0,4);
  sDenpyouCode := sDenpyouCode + Copy(sDate,6,2);
  sDenpyouCode := sDenpyouCode + Copy(sDate,9,2);
}

  sDate := DateToStr(DTP1.Date);
  sDenpyouCode := MakeSDenpyouCode(StrToInt(CBShiiresakiCode.Text),sDate);
  ShowMessage(sDenpyouCode);

  //ShiireShiharaiFlg
  //仕入れ支払いフラグ 仕入れ=0 支払い=1 消費税調整=3
  if CBShiharai.Checked = True then begin
    sShiireShiharaiFlg := '1';
  end else if CBTax.Checked = True then begin
    sShiireShiharaiFlg := '3';
  end else begin
    sShiireShiharaiFlg := '1';
  end;

  //支払い方法
  sShiharaiHouhou := Copy(CBNyukinStatus.Text,0,1);

  //2006.06.16
  cKingaku := StrToCurr(EditKingaku.text);
  cKingaku := cKingaku * -1;

  sSql := 'INSERT INTO ' + CtblTShiireDetail;
  sSql := sSql + ' (';
  sSql := sSql + '  SDenpyouCode ';
  sSql := sSql + ' ,ShiiresakiCode';
  sSql := sSql + ' ,NouhinDate';
  sSql := sSql + ' ,NyuuryokuDate';
  sSql := sSql + ' ,Nyuuryokusha';
  sSql := sSql + ' ,Hyoujijun';
  sSql := sSql + ' ,Code1';
  sSql := sSql + ' ,Code2';
  sSql := sSql + ' ,Suuryou';
  sSql := sSql + ' ,ShiireKingaku';
  sSql := sSql + ' ,Shoukei';
  sSql := sSql + ' ,UpdateDate';
  sSql := sSql + ' ,ShiireShiharaiFlg';
  sSql := sSql + ' ,Gatsubun';
  sSql := sSql + ' ,Memo';
  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + ' (';
  sSql := sSql + '''' + sDenpyouCode + '''' ;
  sSql := sSql + ', ' + CBShiiresakiCode.Text;
  sSql := sSql + ', ''' + sDate + '''' ;
  sSql := sSql + ', ''' + DateToStr(Date()) + '''' ; //入力日
  sSql := sSql + ', ''' + '' + '''' ;                //入力者
  sSql := sSql + ', 0';                              //表示順
  sSql := sSql + ', ''' + '0' + '''' ;               //Code1
  sSql := sSql + ', ''' + '1' + '''' ;               //Code2 商品名=支払い
  sSql := sSql + ', ' + sShiharaiHouhou;             //数量=支払い方法
  sSql := sSql + ', 0';                              //仕入れ金額
  sSql := sSql + ', ' + CurrToStr(cKingaku);           //小計＝支払い金額
  sSql := sSql + ', ''' + DateToStr(Date()) + '''';  //更新日
  sSql := sSql + ', ' + sShiireShiharaiFlg;          //支払い=1 消費税調整=3
  sSql := sSql + ', ''' + EditGatsubun.Text + '/1''';//月分
  sSql := sSql + ', ''' + Memo1.Text + '''';         //備考
  sSql := sSql + ')';


  with QueryINShiireDetail do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;//of with
end;

procedure TfrmShiharaiNyuuryoku.CBTaxClick(Sender: TObject);
begin
  CBShiharai.Checked := False;
end;

procedure TfrmShiharaiNyuuryoku.CBShiharaiClick(Sender: TObject);
begin
  CBTax.Checked := False;
end;



procedure TfrmShiharaiNyuuryoku.DTP1Exit(Sender: TObject);
var
 sCode, sYyyy,sMm : String;
 iYyyy,iMm : Integer;
begin
 //月分
  sYyyy := Copy(DateToStr(DTP1.Date),0,4);
  iYyyy := StrToInt(sYyyy);
  sMm := Copy(DateToStr(DTP1.Date),6,2);
  iMm := StrToInt(sMM);

  iMm := iMm -1;
  if iMm = 0 then begin
    iYyyy := iYyyy-1;
    //2007.02.05
    iMm := 12;
  end;
  sYyyy := IntToStr(iYyyy);
  sMm   := IntToStr(iMm);

  if Length(sMm) = 1 then begin
    sMm := '0' + sMm;
  end;
  EditGatsubun.Text := sYyyy + '/' + sMm;

 //残高を再計算する
 sCode := Copy(CBName.Text, 1, (pos(',', CBName.Text)-1));
 if length(sCode) > 0 then begin
    MakeForm(sCode);
 end;
end;

procedure TfrmShiharaiNyuuryoku.CBTaxExit(Sender: TObject);
begin
  if CBTax.Checked = True then begin
     Memo1.Text := '消費税調整';
     CBNyukinStatus.Text := '5.消費税調整など';
  end;
end;

procedure TfrmShiharaiNyuuryoku.CBShiharaiExit(Sender: TObject);
begin
  if CBShiharai.Checked = True then begin
     Memo1.Text := '';
  end;
end;

procedure TfrmShiharaiNyuuryoku.EditGatsubunExit(Sender: TObject);
var
 sCode : string;
begin
 sCode := Copy(CBName.Text, 1, (pos(',', CBName.Text)-1));
 if length(sCode) > 0 then begin
    MakeForm(sCode);
 end;
end;

end.
