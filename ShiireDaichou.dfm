inherited frmShiireDaichou: TfrmShiireDaichou
  Left = 398
  Top = 126
  Width = 667
  Height = 491
  Caption = 'frmShiireDaichou'
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 651
    inherited Label1: TLabel
      Width = 120
      Caption = #20181#20837#21488#24115#30011#38754
    end
    inherited Label2: TLabel
      Left = 249
      Width = 320
    end
  end
  inherited Panel2: TPanel
    Top = 393
    Width = 651
    inherited BitBtn1: TBitBtn
      Left = 536
    end
    inherited BitBtn2: TBitBtn
      Left = 376
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Width = 651
    Height = 88
    Align = alTop
    object Label6: TLabel
      Left = 116
      Top = 11
      Width = 76
      Height = 13
      Caption = #20181#20837#20808#12467#12540#12489
      Color = clBtnFace
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Left = 300
      Top = 11
      Width = 56
      Height = 13
      Caption = #20181#20837#20808#21517
      Color = clBtnFace
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label8: TLabel
      Left = 260
      Top = 58
      Width = 25
      Height = 13
      Caption = #12363#12425
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 406
      Top = 60
      Width = 26
      Height = 13
      Caption = #12414#12391
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 116
      Top = 35
      Width = 71
      Height = 13
      Caption = #21830#21697#21517#65288'F1)'
      Color = clBtnFace
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbSID: TLabel
      Left = 488
      Top = 35
      Width = 8
      Height = 13
      Caption = '0'
      Color = clBtnFace
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object BitBtn3: TBitBtn
      Left = 28
      Top = 8
      Width = 65
      Height = 61
      Caption = #34920#31034
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlue
      Font.Height = -19
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 550
      Top = 32
      Width = 65
      Height = 61
      Caption = #35201#32004#34920#31034
      Enabled = False
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Visible = False
      OnClick = BitBtn4Click
    end
    object EditShiiresakiCode: TEdit
      Left = 202
      Top = 7
      Width = 77
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clGray
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ImeMode = imClose
      ParentFont = False
      TabOrder = 2
      OnExit = EditShiiresakiCodeExit
    end
    object CBShiiresakiName: TComboBox
      Left = 366
      Top = 8
      Width = 249
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 3
      OnExit = CBShiiresakiNameExit
      OnKeyDown = CBShiiresakiNameKeyDown
    end
    object DTPFrom: TDateTimePicker
      Left = 146
      Top = 56
      Width = 111
      Height = 21
      Date = 36275.451878877310000000
      Time = 36275.451878877310000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object DTPTo: TDateTimePicker
      Left = 288
      Top = 56
      Width = 105
      Height = 21
      Date = 36275.451878877310000000
      Time = 36275.451878877310000000
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object CBItemName: TComboBox
      Left = 201
      Top = 32
      Width = 249
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 6
      OnExit = CBItemNameExit
      OnKeyDown = CBItemNameKeyDown
    end
    object Button1: TButton
      Left = 456
      Top = 56
      Width = 75
      Height = 25
      Caption = #20253#31080#21066#38500
      TabOrder = 7
      OnClick = Button1Click
    end
    object edDenpyouCode: TEdit
      Left = 536
      Top = 58
      Width = 121
      Height = 20
      TabOrder = 8
    end
  end
  inherited SB1: TStatusBar
    Top = 434
    Width = 651
  end
  object SG1: TStringGrid
    Left = 0
    Top = 129
    Width = 651
    Height = 264
    Align = alClient
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
    ParentFont = False
    TabOrder = 4
    OnDrawCell = SG1DrawCell
    OnKeyDown = SG1KeyDown
    OnMouseDown = SG1MouseDown
  end
  object QueryMShiiresaki: TQuery
    DatabaseName = 'taka'
    Left = 172
    Top = 11
  end
  object QuerySG: TQuery
    DatabaseName = 'taka'
    Left = 204
    Top = 11
  end
  object QueryZan: TQuery
    DatabaseName = 'taka'
    Left = 236
    Top = 11
  end
  object QueryItem: TQuery
    DatabaseName = 'taka'
    Left = 260
    Top = 11
  end
  object QueryDel: TQuery
    DatabaseName = 'taka'
    Left = 292
    Top = 11
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 340
    Top = 11
  end
  object Query2: TQuery
    DatabaseName = 'taka'
    Left = 372
    Top = 11
  end
end
