unit Shime;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DB, DBTables;

type
  TfrmShime = class(TfrmMaster)
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CBYyyy: TComboBox;
    CBMm: TComboBox;
    CBShimebi: TComboBox;
    Query1: TQuery;
    Query2: TQuery;
    QueryInsert: TQuery;
    QueryDelete: TQuery;
    QuerySum: TQuery;
    Label4: TLabel;
    EditTokuisakiCode: TEdit;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function GetGatsubunSum(sCode, sYyyy, sMm, sDd, sTax : String) : currency;
    function GetGatsubunTaxChousei(sCode, sYyyy, sMm, sDd : String) : currency;
    function  GetSum(sCode, sFrom, sTo, sShiireShiharaiFlg : String) : Currency;
    function  GetSumTax(sCode, sFrom, sTo : String) : Currency;  //2014.04.01 消費税対応
  public
    { Public declarations }
    procedure PutShimeLine(sCode, sYyyy, sMm, sDd : String);
  end;

var
  frmShime: TfrmShime;

implementation
uses
 Inter, HKLib, DMMaster;
{$R *.dfm}

procedure TfrmShime.BitBtn2Click(Sender: TObject);
var
  sYyyy, sMm, sDd, sSql : string;
  sCode, sName : String;
  i : integer;
begin

//〆日の取得
  sDd := Copy(CBShimebi.Text, 0, Pos(',', CBShimebi.text)-1);

//2006.12.14
  if Trim(EditTokuisakiCode.Text) <> '' then begin
    //確認メッセージ
    if MessageDlg(Trim(EditTokuisakiCode.Text) + 'の〆データを登録しますか?',
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
      Exit;
    end;
    //〆の行の追加
    //消費税
    PutShimeLine(Trim(EditTokuisakiCode.Text), CBYyyy.Text, CBMm.Text, sDd);
    i := 1;
  end else begin

    //確認メッセージ
    if MessageDlg('〆データを登録しますか?',
      mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
      Exit;
    end;

    //仕入先の抽出
    sSql := 'select * from ' + CtblMShiiresaki;
    sSql := sSql + ' where ';
    sSql := sSql + ' Shimebi=' + sDd;
    with Query1 do begin
      Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      i := 0;
      While not EOF do begin
        sCode := FieldByName('Code').AsString;
        sName := FieldByName('Name').AsString;
        SB1.SimpleText := sCode + ' - ' + sName;
        //〆の行の追加
        //消費税
        PutShimeLine(sCode, CBYyyy.Text, CBMm.Text, sDd);
        i := i + 1;
        next;
      end;
     Close;
    end;//of with
  end;//of if
  ShowMessage(IntToStr(i) + ' 件の仕入先を〆ました');
end;

{
  〆の行の定義
  tblTShiireDetail に行を追加する
    SDenpyouCode      = sYyyy + sMm + sDd + 9999999
    ShiiresakiCode    = sCode
    NouhinDate        = sYyyy/sMm/sDd
    NyuuryokuDate     = Date();
    Code1             = 0
    Code2             = 0
    Suuryou           = 0
    ShiireKingaku     = 0
    Shoukei           = 消費税の金額
    UpdateDate　　    = Date();
    ShiireShiharaiFlg = 9;
    Hyoujijun         = 99　　　
}
procedure TfrmShime.PutShimeLine(sCode, sYyyy, sMm, sDd : String);
var
 sSql, sSql2 : String;
 currSum,currSum2,currTax, currTaxC : currency;
 sDenpyouCode, sDate, sDd2 : String;
begin
  if sDd = '0' then begin
    sDd2 := IntToStr(HKLib.GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm)));
  end else begin
    sDd2 := sDd;
  end;
  sDate := sYyyy + '/' + sMm + '/' + sDd2;

  //まず〆の行を削除する
  sSql := 'Delete from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate=''' + sDate + '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' ShiireShiharaiFlg=9';
  with QueryDelete do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;

  //月分の合計を出す
  currSum  := GetGatsubunSum(sCode, sYyyy, sMm, sDd,'');

  //2006.12.14
  currTaxC := GetGatsubunTaxChousei(sCode, sYyyy, sMm, sDd);


  //currTax  := currSum * CsTaxRate; //2014.04.01 消費税対応 before
  currTax  := GetGatsubunSum(sCode, sYyyy, sMm, sDd,'1'); //2014.04.01 消費税対応 after

  currTax  := currTax + currTaxC;

  currSum2 := currSum + currTax;


  //基本データの作成
  sDenpyouCode := 'S' + sDate + '_' + sCode;

  //消費税の行を追加
  sSql := 'INSERT INTO ' + CtblTShiireDetail;
  sSql := sSql + ' (';
  sSql := sSql + 'SDenpyouCode,';
  sSql := sSql + 'ShiiresakiCode,';
  sSql := sSql + 'NouhinDate,';
  sSql := sSql + 'NyuuryokuDate,';
  sSql := sSql + 'Hyoujijun,';
  sSql := sSql + 'Code1,';
  sSql := sSql + 'Code2,';
  sSql := sSql + 'Suuryou,';
  sSql := sSql + 'ShiireKingaku,';
  sSql := sSql + 'Shoukei,';
  sSql := sSql + 'UpdateDate,';
  sSql := sSql + 'ShiireShiharaiFlg';
  sSql := sSql + ') VALUES ( ';
  sSql := sSql + '''' + sDenpyouCode + ''', ' ;
  sSql := sSql +        sCode  +   ', ' ;
  sSql := sSql + '''' + sDate + ''''              + ','; //納品日
  sSql := sSql + '''' + DateToStr(Date()) + ''''  + ','; //入力日
  sSql := sSql +        '0'         + ', ' ; //表示順
  sSql := sSql + '''' + '0'         + ''', ' ; //Code1
  sSql := sSql + '''' + '0'         + ''', ' ; //Code2
  sSql := sSql +        CurrToStr(currSum2)   + ', ' ; //数量       ->合計
  //sSql := sSql +        '0'         + ', ' ;         //仕入れ金額
  sSql := sSql +        CurrToStr(currSum)    + ', ' ; //仕入れ金額 ->税抜き
  sSql := sSql +        CurrToStr(currTax)    + ', ' ; //小計       ->消費税
  sSql := sSql + '''' + DateToStr(Date())  + ''', ' ;  //更新日
  sSql := sSql +        '9'                + ' )' ;    //フラグ

  with QueryInsert do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    ExecSql;
    Close;
  end;

end;

//月分の合計を出す
//sCode : 仕入先コード
// sYyyy, sMm　集計月
// sDd は、〆日
function TfrmShime.GetGatsubunSum(sCode, sYyyy, sMm, sDd, sTax : String) : currency;
var
 sFrom, sTo, sSql, sGetsumatsu : String;
 iYyyy, iMm, iDd : Integer;
 iYyyy2, iMm2, iDd2 : Integer;
 sYyyy2, sMm2, sDd2 : String;
 currSum : Currency;
begin
  //集計期間を求める
  iYyyy := StrToInt(sYyyy);
  iMm   := StrToInt(sMm);
  iDd   := StrToInt(sDd);

  if sDd = '0' then begin  //月末の場合
    sGetsumatsu := IntToStr(HKLib.GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm)));
    sFrom := sYyyy + '/' + sMm + '/01';
    sTo   := sYyyy + '/' + sMm + '/' + sGetsumatsu;

    if(sTax = '1') then begin    //2014.04.01 消費税対応
        currSum := GetSumTax(sCode, sFrom, sTo); //2014.04.01 消費税対応
    end else begin
        currSum := GetSum(sCode, sFrom, sTo, '0');
    end;

  end else begin
    iDd2 := iDd + 1;
    iMm2 := iMm - 1;
    //2007.01.24
    //if iMm = 0 then begin
    if iMm2 = 0 then begin
      iMm2 := 12;
      iYyyy2 := iYyyy-1;
    end else begin
      iYyyy2 := iYyyy;
    end;

    sFrom := IntToStr(iYyyy2) + '/' + IntToStr(iMm2) + '/' + IntToStr(iDd2);
    sTo := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/' + IntToStr(iDd);

    if(sTax = '1') then begin    //2014.04.01 消費税対応
        currSum := GetSumTax(sCode, sFrom, sTo); //2014.04.01 消費税対応
    end else begin
        currSum := GetSum(sCode, sFrom, sTo, '0');
    end;

  end;
  //2006.11.12
  //関数を出たあとで処理する
  //currSum := currSum * CsTaxRate;
  result := currSum;
end;

//月分の消費税の調整を返す
//sCode : 仕入先コード
// sYyyy, sMm　集計月
// sDd は、〆日
function TfrmShime.GetGatsubunTaxChousei(sCode, sYyyy, sMm, sDd : String) : currency;
var
 sFrom, sTo, sSql, sGetsumatsu : String;
 iYyyy, iMm, iDd : Integer;
 iYyyy2, iMm2, iDd2 : Integer;
 sYyyy2, sMm2, sDd2 : String;
 currSum : Currency;
begin
  //集計期間を求める
  iYyyy := StrToInt(sYyyy);
  iMm   := StrToInt(sMm);
  iDd   := StrToInt(sDd);

  if sDd = '0' then begin
    sGetsumatsu := IntToStr(HKLib.GetGetsumatsu(StrToInt(sYyyy), StrToInt(sMm)));
    sFrom := sYyyy + '/' + sMm + '/01';
    sTo   := sYyyy + '/' + sMm + '/' + sGetsumatsu;
    currSum := GetSum(sCode, sFrom, sTo, '3');
  end else begin
    iDd2 := iDd + 1;
    iMm2 := iMm - 1;
    if iMm2 = 0 then begin
      iMm2 := 12;
      iYyyy2 := iYyyy-1;
    end else begin
      iYyyy2 := iYyyy;
    end;
    sFrom := IntToStr(iYyyy2) + '/' + IntToStr(iMm2) + '/' + IntToStr(iDd2);
    sTo := IntToStr(iYyyy) + '/' + IntToStr(iMm) + '/' + IntToStr(iDd);
    currSum := GetSum(sCode, sFrom, sTo, '3');
  end;
  //2006.11.12
  //関数を出たあとで処理する
  //currSum := currSum * CsTaxRate;
  result := currSum;
end;

function  TfrmShime.GetSum(sCode, sFrom, sTo, sShiireShiharaiFlg : String) : Currency;
var
  sSql : String;
  currSum : Currency;
begin
  sSql := 'Select s=sum(Shoukei) from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + sFrom + ''' and ''' + sTo + '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' ShiireShiharaiFlg = ' + sShiireShiharaiFlg;

  with QuerySum do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    currSum := FieldByName('s').asCurrency;
    Close;
  end;
  result := currSum;
end;

//2014.04.01 消費税対応 start
function  TfrmShime.GetSumTax(sCode, sFrom, sTo :String) : Currency;
var
  sSql : String;
  currSum : Currency;
  curShoukei: Currency; //2014.04.01 消費税対応
  douTax  :Double;    //2014.04.01 消費税対応
begin

  //2014.04.01 消費税対応  OLD

{
  sSql := 'Select NouhinDate, Shoukei from ' + CtblTShiireDetail;
  sSql := sSql + ' where ';
  sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' NouhinDate between ''' + sFrom + ''' and ''' + sTo + '''';
  sSql := sSql + ' and ';
  sSql := sSql + ' ShiireShiharaiFlg = 0';

  currSum := 0 ;
  with QuerySum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
      ;

        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;

      while not EOF do begin
        curShoukei := FieldByName('Shoukei').AsFloat;
        if FieldbyName('NouhinDate').AsString  < CsChangeTaxDate then begin
          douTax :=  CsOldTaxRate;
        end else begin
          douTax :=  CsTaxRate;
        end;
        currSum      := currSum + Round(curShoukei * douTax);

        next;
      end
  end;

  }

  //2014.04.01 消費税対応  NEW
   sSql := 'SELECT';
   sSql := sSql + ' SUM(Shoukei) as Soukei,';
   sSql := sSql + ' SUM(CASE WHEN NouhinDate < ''' + CsChangeTaxDate + ''' then Shoukei * ' + FloatToStr(CsOldTaxRate) + ' else Shoukei * ' +  FloatToStr(CsTaxRate) + ' end)  as Tax';
   sSql := sSql + ' FROM ' + CtblTShiireDetail;
   sSql := sSql + ' where ';
   sSql := sSql + ' ShiiresakiCode=''' + sCode +  '''';
   sSql := sSql + ' and ';
   sSql := sSql + ' NouhinDate between ''' + sFrom + ''' and ''' + sTo + '''';
   sSql := sSql + ' and ';
   sSql := sSql + ' ShiireShiharaiFlg = 0';

  with QuerySum do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

        // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         //ShowMessage('該当する伝票はありません');
         Exit;
      end;

     currSum := 0;
     currSum := FieldByName('Tax').AsFloat;

  end;


  result := currSum;
end;
//2014.04.01 消費税対応 end

procedure TfrmShime.FormCreate(Sender: TObject);
begin
  top  := 50;
  left := 50;
  width  := 540;
  height := 220;

end;

end.




