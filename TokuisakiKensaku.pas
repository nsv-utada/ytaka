unit TokuisakiKensaku;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids;

type
  TfrmTokuisakiKensaku = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    SG1: TStringGrid;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
  public
    { Public 宣言 }
  end;

  const
	//グリッドの列の定義

  CintNum           = 0;
  CintTokuisakiCode = 1;
  CintTokuisakiName = 2;
  CintEnd           = 3;   //ターミネーター


	GRowCount = 600; //行数

var
  frmTokuisakiKensaku: TfrmTokuisakiKensaku;

implementation

uses inter;

{$R *.DFM}

procedure TfrmTokuisakiKensaku.FormCreate(Sender: TObject);
begin
	Top := 10;
  Left:= 10;
	Width := Screen.Width-80 ;
  Height := Screen.Height-50;
  //SG1.Align := alClient;
  MakeSG;

end;

procedure TfrmTokuisakiKensaku.MakeSG;
var
	i : integer;
begin
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintNum]:= 40;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintTokuisakiCode]:= 90;
    Cells[CintTokuisakiCode, 0] := '得意先コード';

    ColWidths[CintTokuisakiName]:= 100;
    Cells[CintTokuisakiName, 0] := '得意先名';

    i := 1;
    Cells[CintNum, i] := IntToStr(i);
    Cells[CintTokuisakiCode, i] := '1982';
    Cells[CintTokuisakiName, i] := '大森パロパロ';

    i := 2;
    Cells[CintNum, i] := IntToStr(i);
    Cells[CintTokuisakiCode, i] := '001234';
    Cells[CintTokuisakiName, i] := '鷹松屋商店';

  end;
end;


procedure TfrmTokuisakiKensaku.FormResize(Sender: TObject);
begin
  SG1.Align := alClient;
end;

procedure TfrmTokuisakiKensaku.SG1DblClick(Sender: TObject);
begin
  GTokuisakiCode := SG1.Cells[CintTokuisakiCode, 1];
  GTokuisakiName := SG1.Cells[CintTokuisakiName, 1];
	close;
end;

procedure TfrmTokuisakiKensaku.FormActivate(Sender: TObject);
begin
  SG1.Align := alClient;
end;

end.
