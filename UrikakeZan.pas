unit UrikakeZan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, Buttons, ExtCtrls, ComCtrls, Db, DBTables, ShellAPI;

type
  TfrmUrikakeZan = class(TfrmMaster)
    Label3: TLabel;
    QueryTokuisaki: TQuery;
    QueryCalc: TQuery;
    CBYyyy: TComboBox;
    CBMm: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    EditTokuisakiCode: TEdit;
    Label6: TLabel;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private 宣言 }
		function CalcUrikakezandaka(sTokuisakiCode1, sSeikyuushimebi : String):String;
		function CalcUrikakezandaka2(sTokuisakiCode1, sShiharaibi : String):String;
  public
    { Public 宣言 }
  end;

var
  frmUrikakeZan: TfrmUrikakeZan;

implementation
uses
	Inter, HKLib, PasswordDlg;
{$R *.DFM}

//エクセルへ出力ボタンがクリックされた
//
//月末に限定 2000/03/02に高橋さんに確認
procedure TfrmUrikakeZan.BitBtn2Click(Sender: TObject);
var
	sSumUrikake, sSql, sTokuisakiCode1, sLine : String;
 	F : TextFile;
  iShiharaibi : Integer;
begin
	//確認メッセージ
  if MessageDlg('計算しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_Urikakezan);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力
  sLine := CBYyyy.Text + '年'+CBMm.TExt +'月　月末の売掛残高';
  HMakeFile(CFileName_Urikakezan, sLine);
  sLine := '得意先コード１'+ ',' + '得意先コード２' + ',' + '得意先名' + ',' + 'よみ' + ',' + '売掛残高';
  sLine := sLine + ',' + '課' + ',' + '請求締日';
  HMakeFile(CFileName_Urikakezan, sLine);

	SB1.SimpleText := '計算中 ';
	sSql := 'SELECT ShiharaibiD,TokuisakiCode1, TokuisakiCode2, TokuisakiName, TokuisakiNameYomi';
  sSql := sSql + ', Ka, SeikyuuSimebi ';
  sSql := sSql + ' FROM ' + CtblMTokuisaki;

  //得意先コード指定
  if EditTokuisakiCode.Text <> '' then begin
	  sSql := sSql + ' WHERE TokuisakiCode1 = ' + EditTokuisakiCode.Text;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1';

	//得意先ごとにループ
  with QueryTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
    	sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;

      //月末締めかどうかの確認
      //
      iShiharaibi := FieldByName('ShiharaibiD').AsInteger;
      iShiharaibi := FieldByName('SeikyuuSimebi').AsInteger;

      if iShiharaibi = 0 then begin//月末締め
	    	sSumUrikake := CalcUrikakezandaka(sTokuisakiCode1, IntToStr(iShiharaibi));
      end else begin
	    	sSumUrikake := CalcUrikakezandaka2(sTokuisakiCode1, IntToStr(iShiharaibi));
      end;

      sLine := sTokuisakiCode1;
      sLine := sLine + ',' + FieldByName('TokuisakiCode2').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;
      sLine := sLine + ',' + FieldByName('TokuisakiNameYomi').AsString;
      sLine := sLine + ',' + sSumUrikake;

      sLine := sLine + ',' + FieldByName('Ka').AsString;
      sLine := sLine + ',' + FieldByName('SeikyuuSimebi').AsString;

      SB1.SimpleText := sLine;
      SB1.Update;
		  HMakeFile(CFileName_Urikakezan, sLine);
    	Next;
    end;
    Close;
  end;
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '売掛残高.xls', '', SW_SHOW);
end;

//得意先ごとの売掛残高を計算する
function TfrmUrikakeZan.CalcUrikakezandaka(sTokuisakiCode1, sSeikyuushimebi : String):String;
var
	sDate, sSql, sLine : String;
  rSum : real;
begin

	if sSeikyuushimebi = '0' then begin //月末締めの場合
		sDate := CBYyyy.Text + '/' + CBMm.Text + '/' +
           IntToStr(HKLib.GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text)));
  end else begin
		sDate := CBYyyy.Text + '/' + CBMm.Text + '/' +  sSeikyuushimebi;
  end;

	sSql := 'SELECT SumUrikake = SUM(UriageKingaku) FROM '+ CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate <= ''' + sDate + '''';

  with QueryCalc do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //rSum := FieldByName('SumUrikake').AsCurrency;
    rSum := FieldByName('SumUrikake').AsFloat;
    Result := Real2Str(rSum, 0, 0);
    Close;
  end;

end;


//得意先ごとの売掛残高を計算する
//月末締め以外
function TfrmUrikakeZan.CalcUrikakezandaka2(sTokuisakiCode1, sShiharaibi : String):String;
var
	sDateFrom, sDateTo, sSql, sLine, sSum, sSum2, sSum3, sSum4 : String;
  rSum : real;
begin
  //請求締め日までの売掛金額
	sSum := CalcUrikakezandaka(sTokuisakiCode1, sShiharaibi);

  //請求締め日から月末までの入金金額(返品以外)
  sDateFrom := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(StrToInt(sShiharaibi)+1);
	sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' +
           IntToStr(HKLib.GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text)));

	sSql := 'SELECT SumUrikake = SUM(UriageKingaku) FROM '+ CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate BETWEEN ''' + sDateFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sDateTo + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN(''返品'', ''値引'')))' ;
  with QueryCalc do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //rSum := FieldByName('SumUrikake').AsCurrency;
    rSum := FieldByName('SumUrikake').AsFloat;
    Close;
    sSum2 := Real2Str(Trunc(rSum), 0, 0);
  end;

  //請求締め日から月末までの入金金額(返品の金額)
  sDateFrom := CBYyyy.Text + '/' + CBMm.Text + '/' + IntToStr(StrToInt(sShiharaibi)+1);
	sDateTo := CBYyyy.Text + '/' + CBMm.Text + '/' +
           IntToStr(HKLib.GetGetsumatsu(StrToInt(CBYyyy.Text), StrToInt(CBMm.Text)));

	sSql := 'SELECT SumUrikake = SUM(UriageKingaku) FROM '+ CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate BETWEEN ''' + sDateFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sDateTo + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) IN(''返品'', ''値引'')))' ;
  with QueryCalc do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //rSum := FieldByName('SumUrikake').AsCurrency;
    rSum := FieldByName('SumUrikake').AsFloat;
    Close;
    sSum4 := Real2Str(Trunc(rSum), 0, 0);
  end;

  //請求締め日から月末までの売上金額
	sSql := 'SELECT SumUrikake = SUM(UriageKingaku) FROM '+ CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + ' UriageKingaku>0 ';
  sSql := sSql + ' AND ';
  sSql := sSql + ' DDate BETWEEN ''' + sDateFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sDateTo + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Memo <> ';
  sSql := sSql + '''' + CsCTAX + '''';



  with QueryCalc do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    //rSum := FieldByName('SumUrikake').AsCurrency;
    rSum := FieldByName('SumUrikake').AsFloat;
    Close;
    //返品の金額を引く
    rSum := rSum + Str2Real(sSum4);
    sSum3 := Real2Str(Trunc(rSum*CsTaxRate + rSum),0,0);
  end;


  Result := Real2Str(Str2Real(sSum) + Str2Real(sSum2) + Str2Real(sSum3), 0, 0);

end;

procedure TfrmUrikakeZan.FormCreate(Sender: TObject);
var
  wYyyy, wMm, wDd      : Word;
  sYear, sMonth, sDay  : String;
begin
	width := 554;
  height := 180;

   // 年・月を当日の年・月に設定

   DecodeDate(Date(), wYyyy, wMm, wDd);
	 sYear  := IntToStr(wYyyy);
   sMonth := IntToStr(wMm);
   sDay   := IntToStr(wDd);

   CBYyyy.Text := sYear;
   CBMm.Text   := sMonth;

  //2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

end;

procedure TfrmUrikakeZan.FormActivate(Sender: TObject);
begin
  inherited;
//2002.12.28
if GPassWord = CPassword1 then begin
    Beep;
 end else begin
	//ShowMessage('PassWordが違います');
   close;
end;
GPassWord := '';

end;

end.
